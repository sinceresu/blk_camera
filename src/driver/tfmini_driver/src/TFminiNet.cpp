#include <TFminiNet.h>

namespace benewake
{
    TFminiNet::TFminiNet(const ros::NodeHandle &nh):nh_(nh){
        nh_.param<std::string>("device_ip", device_ip, "192.168.1.4");
        nh_.param<int>("device_port", device_port, 1001);
        std::cout << "ip:" << device_ip << " port:" << device_port << std::endl;
        pub_range = nh_.advertise<sensor_msgs::Range>("/TFmini", 1, true);
        TFmini_range.radiation_type = sensor_msgs::Range::INFRARED;
        TFmini_range.field_of_view = 0.04;
        TFmini_range.min_range = 0.3;
        TFmini_range.max_range = 12;
        TFmini_range.header.frame_id = "/TFmini";
        float dist = 0;
        // tcp_ptr = std::make_shared<EpollTcpClient>(device_ip, device_port);
        // if (!tcp_ptr) {
        //     std::cout << "tcp_client create faield!" << std::endl;
        //     ros::shutdown();
        // }
        socket_ptr = std::make_shared<CSocketConnect>();
        if (!socket_ptr) {
            std::cout << "tcp_client create faield!" << std::endl;
            ros::shutdown();
        }
        else
        {
            if(!socket_ptr->device_connect(device_ip, device_port, 6))
            {
                std::cout << "tcp_client device_connect faield!" << std::endl;
                ros::shutdown();
            }
            else
            {
                std::thread recv_data(std::bind(&TFminiNet::recv_data_thread, this, nullptr));
                recv_data.detach();
            }
        }
        // auto recv_call = [&](const PacketPtr& data) -> void {
        //     // just print recv data to stdout
        //     std::vector<unsigned char> rv;
        //     char *p=(char*)data->msg.c_str();
        //     int sum = 0;
        //     for(int i = 0; i < sizeof(p); i++){
        //         sum += p[i];
        //     }
        //     sum = sum % 256;
        //     rv.insert(rv.end(),p,p+data->msg.size());
        //     // que_mtx.lock();
        //     // receive_msg.push_back(rv);
        //     // while (receive_msg.size() > 1000)
        //     //     receive_msg.pop_front();
        //     // que_mtx.unlock();
        //     if (rv.size() == 9 && rv[0] == 0x59 && rv[1] == 0x59)
        //     {
        //         que_mtx.lock();
        //         receive_msg.push_back(rv);
        //         while (receive_msg.size() > 100)
        //             receive_msg.pop_front();
        //         que_mtx.unlock();
        //     }
        //     return;
        // };
        // tcp_ptr->RegisterOnRecvCallback(recv_call);
        // if (!tcp_ptr->Start()) {
        //     std::cout << "tcp_client start failed!" << std::endl;
        //     ros::shutdown();
        // }
        std::cout << "############tcp_client started!################" << std::endl;
    }

    float TFminiNet::getDist(){
        vector<unsigned char> data = queue_pop();
        if(data.size() != 9) return -1; 
        if(data[0] == 0x59 && data[1] == 0x59){
            int sumCheck = 0;
            for(int i = 0; i < 8; i++)
            {
                sumCheck += data[i];
                //printf(" %x ",data[i]);
            }
            sumCheck = sumCheck % 256;
            //printf("%x %x \n",sumCheck,dataBuf[8]);
            if(sumCheck == data[8])
            {
                return ((float)(data[3] << 8 | data[2]) / 100.0);
            }
            else
            {
                return 0.0;
            }
        }
        return -1;
    }
    bool TFminiNet::getDist(float &_dist)
    {
        _dist = -1;
        bool bRet = false;
        vector<unsigned char> data = queue_pop();
        if(data.size() > 0 && data[0] == 0x59 && data[1] == 0x59)
        {
            int sumCheck = 0;
            for(int i = 0; i < 8; i++)
            {
                sumCheck += data[i];
            }
            sumCheck = sumCheck % 256;
            if(sumCheck == data[8])
            {
                _dist = ((float)(data[3] << 8 | data[2]) / 100.0);
            }
            else
            {
                _dist = 0.0;
            }
            bRet = true;
        }

        return bRet;
    }

    void TFminiNet::closePort(){
        // if(tcp_ptr)
        //     tcp_ptr->Stop();
    }

    void TFminiNet::update(){
        // dist = getDist();
        // if(dist > 0 && dist < TFmini_range.max_range)
        // {
        //     TFmini_range.range = dist;
        //     TFmini_range.header.stamp = ros::Time::now();
        //     pub_range.publish(TFmini_range);
        // }
        if (socket_ptr->GetConnectStatus() >= 0){
            if (recv_fail_count > 12 * 10){
                recv_fail_count = 0;
                socket_ptr->SetConnectStatus(-1);
            }
        } else {
            recv_fail_count = 0;
            return;
        }
        recv_fail_count++;
        // 
        if (getDist(dist))
        {
            if(dist > 0 && dist < TFmini_range.max_range)
            {
                TFmini_range.range = dist;
                TFmini_range.header.stamp = ros::Time::now();
                pub_range.publish(TFmini_range);
            }
            // 
            recv_fail_count = 0;
        }
        
    }

    // bool TFminiNet::readData(unsigned char *_buf, int _nRead){
    //     return true;
    // }
    bool TFminiNet::socket_send_msg(unsigned char *_buf, int _nRead)
    {
        return true;
    }

    void TFminiNet::recv_data_thread(void *p)
    {
        usleep(1000*100);
        ros::Rate loop_rate(1000);
        while (ros::ok())
        {
            if (!socket_ptr) {
                // 
            }
            else
            {
                unsigned char recvBuf[1024];
                int recvLen = 1024;
                if (socket_ptr->recvData(recvBuf, recvLen))
                {
                    std::string msg((char*)recvBuf, recvLen);
                    PacketPtr data = std::make_shared<Packet>(-1, msg);
                    std::vector<unsigned char> rv;
                    char *p=(char*)data->msg.c_str();
                    int sum = 0;
                    for(int i = 0; i < sizeof(p); i++){
                        sum += p[i];
                    }
                    sum = sum % 256;
                    rv.insert(rv.end(),p,p+data->msg.size());
                    // if (rv.size() == 9 && rv[0] == 0x59 && rv[1] == 0x59)
                    if (rv.size() > 0 && rv[0] == 0x59 && rv[1] == 0x59)
                    {
                        que_mtx.lock();
                        receive_msg.push_back(rv);
                        while (receive_msg.size() > 100)
                            receive_msg.pop_front();
                        que_mtx.unlock();
                    }
                }
            }
        }

        return;
    }
}