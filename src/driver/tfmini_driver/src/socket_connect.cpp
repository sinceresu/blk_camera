#include "socket_connect.hpp"

namespace socket_connect
{

    CSocketConnect::CSocketConnect()
        : sock_client(-1), connect_status(-1), b_heart_beat(false), data_length(0)
    {
        counter++;
    }

    CSocketConnect::CSocketConnect(std::string _ip, int _port)
        : sock_client(-1), connect_status(-1), b_heart_beat(false), device_ip(_ip), device_port(_port), data_length(0)
    {
        counter++;
        // 建立连接
        device_connect(_ip, _port);
    }

    CSocketConnect::~CSocketConnect()
    {
        // 断开连接
        device_disconnect();
        //
        counter--;
    }

    unsigned int CSocketConnect::counter = 0;

    bool CSocketConnect::device_connect(std::string _ip, int _port, unsigned int _sec)
    {
        bool bRet = false;
        //
        device_ip = _ip;
        device_port = _port;
        //
        connect_status = -1;
        // 初始化
        int sockClient = socket(AF_INET, SOCK_STREAM, 0);
        if (sockClient != -1)
        {
            // 设置超时时间
            struct timeval tv_timeout;
            tv_timeout.tv_sec = 6;
            tv_timeout.tv_usec = 0;
            setsockopt(sockClient, SOL_SOCKET, SO_SNDTIMEO, &tv_timeout, sizeof(tv_timeout));
            setsockopt(sockClient, SOL_SOCKET, SO_RCVTIMEO, &tv_timeout, sizeof(tv_timeout));
            // 建立连接
            struct sockaddr_in addrSrv;
            addrSrv.sin_family = AF_INET;
            addrSrv.sin_addr.s_addr = inet_addr(_ip.c_str());
            addrSrv.sin_port = htons(_port);
            //
            int nRes = connect(sockClient, (struct sockaddr *)&addrSrv, sizeof(addrSrv));
            if (nRes == -1)
            {
                close(sockClient);
                sockClient = -1;
                connect_status = -1;
                bRet = false;
            }
            else
            {
                connect_status = 0;
                bRet = true;
            }
        }
        else
        {
            sockClient = -1;
            bRet = false;
        }
        sock_client = sockClient;

        set_reconnection(_sec);

        return bRet;
    }

    void CSocketConnect::device_disconnect()
    {
        b_heart_beat = false;
        // 关闭连接
        if (sock_client >= 0)
        {
            close(sock_client);
            sock_client = -1;
        }
        connect_status = -1;
    }

    void CSocketConnect::set_reconnection(unsigned int sec)
    {
        if (!b_heart_beat && sec > 0)
        {
            b_heart_beat = true;

            std::thread heart_beat(std::bind(&CSocketConnect::heart_beat, this, sec));
            heart_beat.detach();
        }
    }

    void CSocketConnect::heart_beat(unsigned int sec)
    {
        sleep(sec);

        int failed_count = 0;
        while (b_heart_beat)
        {
            if (sock_client < 0 || connect_status < 0)
            {
                if (sock_client >= 0)
                {
                    close(sock_client);
                    sock_client = -1;
                    connect_status = -1;
                }
                //
                printf("reconnection. \r\n");
                //
                bool b_ret = device_connect(device_ip, device_port);
                if (!b_ret || sock_client < 0 || connect_status < 0)
                {
                    sleep(sec);
                    continue;
                }
            }

            sleep(sec);
        }
    }

    bool CSocketConnect::sendData(const unsigned char *cmdBuf, const unsigned int cmdLen)
    {
        if (sock_client < 0)
        {
            return false;
        }

        printf("send: ");
        for (int i = 0; i < cmdLen; i++)
        {
            printf("%02x ", cmdBuf[i]);
        }
        printf("\r\n");

        send_mtx.lock();

        bool bRet = false;
        try
        {
            // 发送命令
            int size = send(sock_client, (const char *)cmdBuf, cmdLen, 0);
            if (size > 0)
            {
                send_fail_count = 0;
                bRet = true;
                // unsigned char recvBuf[128];
                // memset(recvBuf, 0x00, 128);
                // //等待应答
                // size = recv(sock_client, (char *)&recvBuf, 128, 0);
                // if (size > 0)
                // {
                //     bRet = true;
                // }
            }
            else
            {
                send_fail_count++;
            }
        }
        catch (const std::exception &e)
        {
            send_fail_count++;
            std::cerr << e.what() << '\n';
        }

        send_mtx.unlock();

        return bRet;
    }

    bool CSocketConnect::recvData(unsigned char *recvBuf, int &recvLen)
    {
        if (sock_client < 0)
        {
            return false;
        }
        recv_mtx.lock();

        bool bRet = false;
        try
        {
            //等待应答
            int size = recv(sock_client, (char *)recvBuf, recvLen, 0);
            if (size > 0)
            {
                printf("recv: ");
                for (int i = 0; i < size; i++)
                {
                    printf("%02x ", recvBuf[i]);
                }
                printf("\r\n");

                recv_fail_count = 0;

                recvLen = size;
                bRet = true;
            }
            else
            {
                recv_fail_count++;
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
        }

        recv_mtx.unlock();

        return bRet;
    }

    std::string CSocketConnect::GetDeviceIp()
    {
        return device_ip;
    }

    int CSocketConnect::GetDevicePort()
    {
        return device_port;
    }

    int CSocketConnect::GetConnectStatus()
    {
        return connect_status;
    }

    int CSocketConnect::SetConnectStatus(int status)
    {
        connect_status = status;
        return connect_status;
    }

}