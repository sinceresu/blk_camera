#ifndef socket_connect_HPP
#define socket_connect_HPP

#pragma once
#include <thread>
#include <mutex>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include <unistd.h>
#include <string.h>
#include <cstdlib>
#include <iostream>

#define DATA_GET_HIGH_BYTE(data) (((data) >> 8) & 0xFF)
#define DATA_GET_LOW_BYTE(data) ((data)&0xFF)

namespace socket_connect
{

    enum
    {
        CMD01 = 0x0A,
        CMD02 = 0x0B,
        CMD03 = 0x0C,
        CMD04 = 0x0D,
        CMD05 = 0x0E,
        CMD06 = 0x0F,
        CMD07 = 0x20,
        CMD08 = 0x21,
        CMD09 = 0x27,
        CMD10 = 0x40,
        CMD11 = 0x41,
        CMD12 = 0x50,
        CMD13 = 0x51,
        CMD14 = 0x52,
        CMD15 = 0x53,
        CMD16 = 0x54,
        CMD17 = 0x55,
        CMD18 = 0x56,
        CMD19 = 0x57,
    };

    typedef std::function<void(const char *data, int lenght)> DataCallback;

    class CSocketConnect
    {
    public:
        CSocketConnect();
        CSocketConnect(std::string _ip, int _port);
        ~CSocketConnect();

    private:
        std::string device_ip;
        int device_port;
        //
        int sock_client;
        int connect_status;
        bool b_heart_beat;
        static unsigned int counter;
        //
        unsigned int data_length;
        //
        std::mutex send_mtx;
        std::mutex recv_mtx;
        //
        unsigned int send_fail_count;
        unsigned int recv_fail_count;

    private:
        void heart_beat(unsigned int sec = 0);

    public:
        bool device_connect(std::string _ip, int _port, unsigned int _sec = 0);
        void device_disconnect();
        void set_reconnection(unsigned int sec = 0);
        bool sendData(const unsigned char *cmdBuf, const unsigned int cmdLen);
        bool recvData(unsigned char *recvBuf, int &recvLen);
        //
        std::string GetDeviceIp();
        int GetDevicePort();
        int GetConnectStatus();
        int SetConnectStatus(int status);
        //
        bool ResetDevicePositionX();
    };

}

#endif