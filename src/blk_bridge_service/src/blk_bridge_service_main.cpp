#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "blk_bridge_service_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace blk_bridge_service
{
    void run()
    {
        CBridgeServiceNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);

        ros::param::get("~matt_address", node_options.matt_address);
        ros::param::get("~matt_clientId", node_options.matt_clientId);
        ros::param::get("~matt_username", node_options.matt_username);
        ros::param::get("~matt_password", node_options.matt_password);

        ros::param::get("~work_mode", node_options.work_mode);
        ros::param::get("~ptz_pose", node_options.ptz_pose);
        ros::param::get("~radar_data", node_options.radar_data);
        ros::param::get("~ptz_visiable", node_options.ptz_visiable);
        ros::param::get("~ptz_infrared", node_options.ptz_infrared);
        ros::param::get("~ptz_temperature", node_options.ptz_temperature);
        ros::param::get("~ptz_radar", node_options.ptz_radar);

        CBridgeServiceNode blk_bridge_service_node(node_options);

        ROS_INFO("message bridge service node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("blk_bridge_service");

    ros::init(argc, argv, "blk_bridge_service");
    ros::Time::init();

    blk_bridge_service::run();

    ros::shutdown();

    return 0;
}