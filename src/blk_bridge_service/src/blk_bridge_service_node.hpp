#ifndef blk_bridge_service_node_HPP
#define blk_bridge_service_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/Range.h"
#include "data_sync_msgs/VisiablePTZ.h"
#include "data_sync_msgs/InfraredPTZ.h"
#include "data_sync_msgs/TemperaturePTZ.h"
#include "data_sync_msgs/RadarPTZ.h"
#include "blk_control_msgs/WorkMode.h"

#include <jsoncpp/json/json.h>

#include "blk_bridge_service.hpp"
#include "Common.hpp"
#include "mqtt_base.hpp"
#include "websocket_base.hpp"
#include "ros_structs.hpp"
#include "mqtt_structs.hpp"

namespace blk_bridge_service
{
    class CBridgeServiceNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;
            //
            std::string matt_address;
            std::string matt_clientId;
            std::string matt_username;
            std::string matt_password;
            //
            std::string work_mode;
            std::string ptz_pose;
            std::string radar_data;
            std::string ptz_visiable;
            std::string ptz_infrared;
            std::string ptz_temperature;
            std::string ptz_radar;
        } NodeOptions;

    public:
        CBridgeServiceNode(NodeOptions _node_options);
        ~CBridgeServiceNode();

    private:
        NodeOptions node_options;
        CBridgeService bridge_service;

        std::queue<blk_control_msgs::WorkMode> q_work_mode;
        std::queue<nav_msgs::Odometry> q_ptz_pose;
        std::queue<sensor_msgs::Range> q_radar_data;
        std::queue<data_sync_msgs::VisiablePTZ> q_ptz_visiable;
        std::queue<data_sync_msgs::InfraredPTZ> q_ptz_infrared;
        std::queue<data_sync_msgs::TemperaturePTZ> q_ptz_temperature;
        std::queue<data_sync_msgs::RadarPTZ> q_ptz_radar;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        ros::Publisher test_Publisher;

        // Subscriber
        ros::Subscriber work_mode_Subscriber;
        void work_mode_Callback(const blk_control_msgs::WorkMode::ConstPtr &_msg);
        ros::Subscriber ptz_pose_subscriber;
        void ptz_pose_Callback(const nav_msgs::Odometry::ConstPtr &msg);
        ros::Subscriber radar_data_subscriber;
        void radar_data_Callback(const sensor_msgs::Range::ConstPtr &msg);
        ros::Subscriber ptz_visiable_Subscriber;
        void ptz_visiable_Callback(const data_sync_msgs::VisiablePTZ::ConstPtr &_msg);
        ros::Subscriber ptz_infrared_Subscriber;
        void ptz_infrared_Callback(const data_sync_msgs::InfraredPTZ::ConstPtr &_msg);
        ros::Subscriber ptz_temperature_Subscriber;
        void ptz_temperature_Callback(const data_sync_msgs::TemperaturePTZ::ConstPtr &_msg);
        ros::Subscriber ptz_radar_Subscriber;
        void ptz_radar_Callback(const data_sync_msgs::RadarPTZ::ConstPtr &_msg);

        void LaunchPublishers();
        void LaunchSubscribers();

    private:
        CMqttBase mqtt_base;
        int mqtt_init(std::string _address, std::string _clientId,
                      std::string _username, std::string _password);
        void test_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);

        int test_mqttPublish(std::string data);

        void MqttPublishers();
        void MqttSubscribers();

    private:
        CWebSocketBase websocket_base;
        int websocket_init();
        void websocket_message(websocketpp::connection_hdl hdl, server::message_ptr msg);
        std::mutex msg_mtx;
        std::queue<message_t> message_list;
        void websocket_message_thread(void *p);

    private:
        int msgRosToMqtt(const std_msgs::String &ros_msg, std::string &mqtt_msg);
        int msgMqttToRos(const std::string &mqtt_msg, std_msgs::String &ros_msg);
    };
}

#endif