#ifndef websocket_base_HPP
#define websocket_base_HPP

#pragma once
#define ASIO_STANDALONE
#include "websocketpp/config/asio_no_tls.hpp"
#include "websocketpp/server.hpp"

#include <map>
#include <set>
#include <queue>
#include <functional>

#define WEBSOCKET_BASE_VERSION "v1.0"

namespace blk_bridge_service
{

    typedef websocketpp::server<websocketpp::config::asio> server;

    typedef std::set<websocketpp::connection_hdl, std::owner_less<websocketpp::connection_hdl>> con_list;

    typedef std::function<void(websocketpp::connection_hdl hdl, server::message_ptr)> websocketMessageCallback;

    typedef struct _message_t
    {
        websocketpp::connection_hdl hdl;
        server::message_ptr msg;
    } message_t;

    class CWebSocketBase
    {
    public:
        CWebSocketBase();
        ~CWebSocketBase();

    private:
        server m_endpoint;
        void echo_open_handler(websocketpp::connection_hdl hdl);
        void echo_close_handler(websocketpp::connection_hdl hdl);
        void echo_fail_handler(websocketpp::connection_hdl hdl);
        void echo_message_handler(websocketpp::connection_hdl hdl, server::message_ptr msg);

        con_list m_connection_hdl;
        websocketMessageCallback callback_fun;

    public:
        void run(int port = 9002);
        void stop();
        int send_all(std::string const &payload, websocketpp::frame::opcode::value op);
        int send(websocketpp::connection_hdl hdl, std::string const &payload, websocketpp::frame::opcode::value op);
        void registerCallback(websocketMessageCallback callback);
    };

}

#endif
