#ifndef ros_structs_HPP
#define ros_structs_HPP

#pragma once
#include <string>
#include <vector>
#include "jsoncpp/json/json.h"
#include "x2struct/x2struct.hpp"

// ros struct
typedef struct RosInt
{
    int data;
    XTOSTRUCT(O(data));
} SRosInt, *PRosInt;

typedef struct RosString
{
    std::string data;
    XTOSTRUCT(O(data));
} SRosString, *PRosString;

typedef struct RosBool
{
    bool data;
    XTOSTRUCT(O(data));
} SRosBool, *PRosBool;

typedef struct RosTime
{
    unsigned int secs;
    unsigned int nsecs;
    XTOSTRUCT(O(secs, nsecs));
} SRosTime, *PRosTime;

typedef struct RosHeader
{
    unsigned int seq;
    SRosTime stamp;
    std::string frame_id;
    XTOSTRUCT(O(seq, stamp, frame_id));
} SRosHeader, *PRosHeader;

typedef struct RosPoint
{
    double x;
    double y;
    double z;
    RosPoint() : x(0), y(0), z(0) {}
    XTOSTRUCT(O(x, y, z));
} SRosPoint, *PRosPoint;

typedef struct RosQuaternion
{
    double x;
    double y;
    double z;
    double w;
    RosQuaternion() : x(0), y(0), z(0), w(0) {}
    XTOSTRUCT(O(x, y, z, w));
} SRosQuaternion, *PRosQuaternion;

typedef struct RosPose
{
    SRosPoint position;
    SRosQuaternion orientation;
    XTOSTRUCT(O(position, orientation));
} SRosPose, *PRosPose;

typedef struct RosPoseWithCovariance
{
    SRosPose pose;
    // double covariance[36];
    std::vector<double> covariance;
    XTOSTRUCT(O(pose, covariance));
} SRosPoseWithCovariance, *PRosPoseWithCovariance;

typedef struct RosVector3
{
    double x;
    double y;
    double z;
    RosVector3() : x(0), y(0), z(0) {}
    XTOSTRUCT(O(x, y, z));
} SRosVector3, *PRosVector3;

typedef struct RosTwist
{
    SRosVector3 linear;
    SRosVector3 angular;
    XTOSTRUCT(O(linear, angular));
} SRosTwist, *PRosTwist;

typedef struct RosTwistWithCovariance
{
    SRosTwist twist;
    // double covariance[36];
    std::vector<double> covariance;
    XTOSTRUCT(O(twist, covariance));
} SRosTwistWithCovariance, *PRosTwistWithCovariance;

typedef struct RosOdometry
{
    SRosHeader header;
    std::string child_frame_id;
    SRosPoseWithCovariance pose;
    SRosTwistWithCovariance twist;
    XTOSTRUCT(O(header, child_frame_id, pose, twist));
} SRosOdometry, *PRosOdometry;

typedef struct RosPoseWithCovarianceStamped
{
    SRosHeader header;
    SRosPoseWithCovariance pose;
    XTOSTRUCT(O(header, pose));
} SRosPoseWithCovarianceStamped, *PRosPoseWithCovarianceStamped;

typedef struct RosKeyValue
{
    std::string key;
    std::string value;
    XTOSTRUCT(O(key, value));
} SRosKeyValue, *PRosKeyValue;

typedef struct RosDiagnosticStatus
{
    unsigned char level;
    std::string name;
    std::string message;
    std::string hardware_id;
    std::vector<SRosKeyValue> values;
    XTOSTRUCT(O(level, name, message, hardware_id, values));
} SRosDiagnosticStatus, *PRosDiagnosticStatus;

typedef struct RosDiagnosticArray
{
    SRosHeader header;
    std::vector<SRosDiagnosticStatus> status;
    XTOSTRUCT(O(header, status));
} SRosDiagnosticArray, *PRosDiagnosticArray;

typedef struct RosTransform
{
    SRosVector3 translation;
    SRosQuaternion rotation;
    XTOSTRUCT(O(translation, rotation));
} SRosTransform, *PRosTransform;

typedef struct RosTransformStamped
{
    SRosHeader header;
    std::string child_frame_id;
    SRosTransform transform;
    XTOSTRUCT(O(header, child_frame_id, transform));
} SRosTransformStamped, *PRosTransformStamped;

typedef struct RosTFMessage
{
    std::vector<SRosTransformStamped> transforms;
    XTOSTRUCT(O(transforms));
} SRosTFMessage, *PRosTFMessage;

typedef struct RosPoseStamped
{
    SRosHeader header;
    SRosPose pose;
    XTOSTRUCT(O(header, pose));
} SRosPoseStamped, *PRosPoseStamped;

typedef struct RosPoint32
{
    float x;
    float y;
    float z;
    RosPoint32() : x(0), y(0), z(0) {}
    XTOSTRUCT(O(x, y, z));
} SRosPoint32, *PRosPoint32;

typedef struct RosPolygon
{
    std::vector<SRosPoint32> points;
    XTOSTRUCT(O(points));
} SRosPolygon, *PRosPolygon;

typedef struct RosImage
{
    SRosHeader header;
    unsigned int width;
    unsigned int height;
    std::string encoding;
    unsigned char is_bigendian;
    unsigned int step;
    // std::vector<unsigned char> data;
    std::string data;
    XTOSTRUCT(O(header, width, height, encoding, is_bigendian, step, data));
} SRosImage, *PRosImage;

// custom struct

#endif
