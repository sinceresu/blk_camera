#ifndef blk_bridge_service_HPP
#define blk_bridge_service_HPP

#pragma once
#include <queue>
#include <mutex>
#include <lz4.h>
#include <jsoncpp/json/json.h>

namespace blk_bridge_service
{
    class CBridgeService
    {
    public:
        CBridgeService();
        ~CBridgeService();

    public:
        bool zipBuff(const char *_buff, int _len, char **_outBuff, int &_outLen);
        bool unZipBuff(const char *_buff, int _len, char *_outBuff, int _outLen);
    };
}

#endif