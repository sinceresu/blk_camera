#include "websocket_base.hpp"

namespace blk_bridge_service
{

  CWebSocketBase::CWebSocketBase()
  {
    // Set logging settings
    // m_endpoint.set_error_channels(websocketpp::log::elevel::all);
    // m_endpoint.set_access_channels(websocketpp::log::alevel::all ^ websocketpp::log::alevel::frame_payload);
    m_endpoint.set_error_channels(websocketpp::log::elevel::all);
    m_endpoint.set_access_channels(websocketpp::log::alevel::none);

    // Initialize Asio
    m_endpoint.init_asio();

    m_endpoint.set_open_handler(std::bind(&CWebSocketBase::echo_open_handler, this,
                                          std::placeholders::_1));
    m_endpoint.set_close_handler(std::bind(&CWebSocketBase::echo_close_handler, this,
                                           std::placeholders::_1));
    m_endpoint.set_fail_handler(std::bind(&CWebSocketBase::echo_fail_handler, this,
                                          std::placeholders::_1));

    // Set the default message handler to the echo handler
    m_endpoint.set_message_handler(std::bind(&CWebSocketBase::echo_message_handler, this,
                                             std::placeholders::_1, std::placeholders::_2));
  }

  CWebSocketBase::~CWebSocketBase()
  {
    m_endpoint.stop();
  }

  void CWebSocketBase::echo_open_handler(websocketpp::connection_hdl hdl)
  {
    m_connection_hdl.insert(hdl);

    server::connection_ptr con = m_endpoint.get_con_from_hdl(hdl);
    std::string remote_str = con->get_remote_endpoint();

    std::cout << "> echo_open_handler [info] " << remote_str << std::endl;
  }

  void CWebSocketBase::echo_close_handler(websocketpp::connection_hdl hdl)
  {
    if (m_connection_hdl.size() > 0)
    {
      m_connection_hdl.erase(hdl);
    }

    server::connection_ptr con = m_endpoint.get_con_from_hdl(hdl);
    std::string remote_str = con->get_remote_endpoint();

    std::cout << "> echo_close_handler [info] " << remote_str << std::endl;
  }

  void CWebSocketBase::echo_fail_handler(websocketpp::connection_hdl hdl)
  {
    server::connection_ptr con = m_endpoint.get_con_from_hdl(hdl);
    std::string remote_str = con->get_remote_endpoint();

    std::cout << "> echo_fail_handler [info] " << remote_str << std::endl;
  }

  void CWebSocketBase::echo_message_handler(websocketpp::connection_hdl hdl, server::message_ptr msg)
  {
    // // write a new message
    // m_endpoint.send(hdl, msg->get_payload(), msg->get_opcode());

    if (callback_fun)
    {
      callback_fun(hdl, msg);
    }
  }

  void CWebSocketBase::run(int port)
  {
    std::cout << "> start websocket listen [info] port:" << port << std::endl;

    // Listen on port 9002
    // m_endpoint.listen(9002);
    m_endpoint.listen(port);

    // Queues a connection accept operation
    m_endpoint.start_accept();

    // Start the Asio io_service run loop
    m_endpoint.run();
  }

  void CWebSocketBase::stop()
  {
    std::cout << "> stop websocket listen [info]" << std::endl;

    m_endpoint.stop();
  }

  int CWebSocketBase::send_all(std::string const &payload, websocketpp::frame::opcode::value op)
  {
    for (auto it : m_connection_hdl)
    {
      try
      {
        m_endpoint.send(it, payload, op);
      }
      catch (const std::exception &e)
      {
        std::cerr << e.what() << '\n';
      }
    }

    return 0;
  }

  int CWebSocketBase::send(websocketpp::connection_hdl hdl, std::string const &payload, websocketpp::frame::opcode::value op)
  {
    try
    {
      m_endpoint.send(hdl, payload, op);
    }
    catch (const std::exception &e)
    {
      std::cerr << e.what() << '\n';
    }

    return 0;
  }

  void CWebSocketBase::registerCallback(websocketMessageCallback callback)
  {
    callback_fun = callback;
  }
}