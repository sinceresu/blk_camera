#include "blk_bridge_service_node.hpp"

namespace blk_bridge_service
{
    CBridgeServiceNode::CBridgeServiceNode(NodeOptions _node_options)
        : node_options(_node_options)
    {
        // mqtt_init(node_options.matt_address, node_options.matt_clientId,
        //           node_options.matt_username, node_options.matt_password);

        LaunchPublishers();
        LaunchSubscribers();

        websocket_init();
    }

    CBridgeServiceNode::~CBridgeServiceNode()
    {
        websocket_base.stop();
    }

    void CBridgeServiceNode::LaunchPublishers()
    {
        test_Publisher = nh.advertise<std_msgs::String>("/test", 1);
    }

    void CBridgeServiceNode::LaunchSubscribers()
    {
        work_mode_Subscriber = nh.subscribe(node_options.work_mode, 1, &CBridgeServiceNode::work_mode_Callback, this);
        ptz_pose_subscriber = nh.subscribe(node_options.ptz_pose, 1, &CBridgeServiceNode::ptz_pose_Callback, this);
        radar_data_subscriber = nh.subscribe(node_options.radar_data, 1, &CBridgeServiceNode::radar_data_Callback, this);
        ptz_visiable_Subscriber = nh.subscribe(node_options.ptz_visiable, 1, &CBridgeServiceNode::ptz_visiable_Callback, this);
        ptz_infrared_Subscriber = nh.subscribe(node_options.ptz_infrared, 1, &CBridgeServiceNode::ptz_infrared_Callback, this);
        ptz_temperature_Subscriber = nh.subscribe(node_options.ptz_temperature, 1, &CBridgeServiceNode::ptz_temperature_Callback, this);
        ptz_radar_Subscriber = nh.subscribe(node_options.ptz_radar, 1, &CBridgeServiceNode::ptz_radar_Callback, this);
    }

    void CBridgeServiceNode::work_mode_Callback(const blk_control_msgs::WorkMode::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_work_mode.push(*_msg);
        while (q_work_mode.size() > 1)
        {
            q_work_mode.pop();
        }

        Json::Value json_msg;
        json_msg["datatype"] = "workmode";
        json_msg["value"] = _msg->mode;
        std::string msg_str = "";
        msg_str = json_msg.toStyledString();

        // websocket_base.send_all(msg_str, websocketpp::frame::opcode::value::text);
    }

    void CBridgeServiceNode::ptz_pose_Callback(const nav_msgs::Odometry::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_ptz_pose.push(*_msg);
        while (q_ptz_pose.size() > 1)
        {
            q_ptz_pose.pop();
        }
    }

    void CBridgeServiceNode::radar_data_Callback(const sensor_msgs::Range::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_radar_data.push(*_msg);
        while (q_radar_data.size() > 1)
        {
            q_radar_data.pop();
        }
    }

    void CBridgeServiceNode::ptz_visiable_Callback(const data_sync_msgs::VisiablePTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_ptz_visiable.push(*_msg);
        while (q_ptz_visiable.size() > 1)
        {
            q_ptz_visiable.pop();
        }

        Json::Value json_msg;
        json_msg["datatype"] = "visiable";
        json_msg["width"] = std::to_string(_msg->visiable.width);
        json_msg["height"] = std::to_string(_msg->visiable.height);
        json_msg["dataformat"] = "uchar";
        // zip
        // std::string img_zip;
        // char *outBuff = NULL;
        // int outLen = 0;
        // if (bridge_service.zipBuff((const char *)_msg->visiable.data.data(), _msg->visiable.data.size(), &outBuff, outLen))
        // {
        //     img_zip.resize(outLen);
        //     memcpy(&img_zip[0], outBuff, outLen);
        //     delete[] outBuff;
        // }
        // Base64
        std::string img_str;
        img_str.resize(_msg->visiable.data.size());
        memcpy(&img_str[0], _msg->visiable.data.data(), _msg->visiable.data.size());
        std::string img_base64;
        Base64Encode(img_str, &img_base64);
        // Base64Encode(img_zip, &img_base64);
        json_msg["data"] = img_base64;
        json_msg["PTZ"] = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "," + std::to_string(_msg->ptz_pose.pose.pose.position.z);
        std::string msg_str = "";
        msg_str = json_msg.toStyledString();

        websocket_base.send_all(msg_str, websocketpp::frame::opcode::value::text);
    }

    void CBridgeServiceNode::ptz_infrared_Callback(const data_sync_msgs::InfraredPTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_ptz_infrared.push(*_msg);
        while (q_ptz_infrared.size() > 1)
        {
            q_ptz_infrared.pop();
        }

        Json::Value json_msg;
        json_msg["datatype"] = "infrared";
        json_msg["width"] = std::to_string(_msg->infrared.width);
        json_msg["height"] = std::to_string(_msg->infrared.height);
        json_msg["dataformat"] = "float";
        // zip
        // std::string img_zip;
        // char *outBuff = NULL;
        // int outLen = 0;
        // if (bridge_service.zipBuff((const char *)_msg->infrared.data.data(), _msg->infrared.data.size(), &outBuff, outLen))
        // {
        //     img_zip.resize(outLen);
        //     memcpy(&img_zip[0], outBuff, outLen);
        //     delete[] outBuff;
        // }
        //
        // Base64
        std::string img_str;
        img_str.resize(_msg->infrared.data.size());
        memcpy(&img_str[0], _msg->infrared.data.data(), _msg->infrared.data.size());
        std::string img_base64;
        Base64Encode(img_str, &img_base64);
        // Base64Encode(img_zip, &img_base64);
        json_msg["data"] = img_base64;
        json_msg["PTZ"] = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "," + std::to_string(_msg->ptz_pose.pose.pose.position.z);
        std::string msg_str = "";
        msg_str = json_msg.toStyledString();

        websocket_base.send_all(msg_str, websocketpp::frame::opcode::value::text);
    }

    void CBridgeServiceNode::ptz_temperature_Callback(const data_sync_msgs::TemperaturePTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_ptz_temperature.push(*_msg);
        while (q_ptz_temperature.size() > 1)
        {
            q_ptz_temperature.pop();
        }

        Json::Value json_msg;
        json_msg["datatype"] = "temperature";
        json_msg["width"] = std::to_string(_msg->temperature.width);
        json_msg["height"] = std::to_string(_msg->temperature.height);
        json_msg["dataformat"] = "float";
        // zip
        // std::string img_zip;
        // char *outBuff = NULL;
        // int outLen = 0;
        // if (bridge_service.zipBuff((const char *)_msg->temperature.data.data(), _msg->temperature.data.size(), &outBuff, outLen))
        // {
        //     img_zip.resize(outLen);
        //     memcpy(&img_zip[0], outBuff, outLen);
        //     delete[] outBuff;
        // }
        //
        // Base64
        std::string img_str;
        img_str.resize(_msg->temperature.data.size());
        memcpy(&img_str[0], _msg->temperature.data.data(), _msg->temperature.data.size());
        std::string img_base64;
        Base64Encode(img_str, &img_base64);
        // Base64Encode(img_zip, &img_base64);
        json_msg["data"] = img_base64;
        json_msg["PTZ"] = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "," + std::to_string(_msg->ptz_pose.pose.pose.position.z);
        std::string msg_str = "";
        msg_str = json_msg.toStyledString();

        websocket_base.send_all(msg_str, websocketpp::frame::opcode::value::text);
    }

    void CBridgeServiceNode::ptz_radar_Callback(const data_sync_msgs::RadarPTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        q_ptz_radar.push(*_msg);
        while (q_ptz_radar.size() > 1)
        {
            q_ptz_radar.pop();
        }

        Json::Value json_msg;
        json_msg["datatype"] = "radar";
        json_msg["width"] = "";
        json_msg["height"] = "";
        json_msg["dataformat"] = "float";
        json_msg["data"] = std::to_string(_msg->radar.range);
        json_msg["PTZ"] = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "," + std::to_string(_msg->ptz_pose.pose.pose.position.z);
        std::string msg_str = "";
        msg_str = json_msg.toStyledString();

        websocket_base.send_all(msg_str, websocketpp::frame::opcode::value::text);
    }

    // mqtt
    int CBridgeServiceNode::mqtt_init(std::string _address, std::string _clientId,
                                      std::string _username, std::string _password)
    {
        // mqtt
        mqtt_base.init(_address, _clientId);
        mqtt_base.connect(_username, _password);

        // MqttPublishers();
        MqttSubscribers();

        return 0;
    }

    void CBridgeServiceNode::MqttPublishers()
    {
    }

    void CBridgeServiceNode::MqttSubscribers()
    {
        mqtt_base.subscribe("/test", 0,
                            std::bind(&CBridgeServiceNode::test_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    }

    void CBridgeServiceNode::test_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("test_mqttCallback - %s", _message.c_str());

        // SMqttObjectPose mqtt_object_pose;
        // x2struct::X::loadjson(_message, mqtt_object_pose, false);

        std_msgs::String test;
        if (msgMqttToRos(_message, test) == 0)
        {
            test_Publisher.publish(test);
        }
    }

    int CBridgeServiceNode::test_mqttPublish(std::string data)
    {
        Json::Reader reader;
        Json::Value data_json;
        if (!reader.parse(data, data_json))
        {
            std::cout << "json parse error" << std::endl;
            return -1;
        }
        time_t stamp = GetTimeStamp();
        std::string strStamp = std::to_string(stamp);
        std::string strLogId = strStamp;
        char md_c[32 + 1];
        if (md5_encoded(strStamp.c_str(), md_c) == 0)
        {
            strLogId = md_c;
        }
        Json::Value mqtt_json;
        mqtt_json["logId"] = strLogId;
        mqtt_json["gatewayId"] = node_options.device_id;
        mqtt_json["timestamp"] = (Json::Int64)stamp;
        mqtt_json["type"] = 0;
        mqtt_json["version"] = MQTT_BASE_VERSION;
        mqtt_json["data"] = data_json;
        std::string mqtt_str = mqtt_json.toStyledString();

        std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/iot/object/position/up";
        mqtt_base.publish(topic_name, 0, mqtt_str);

        return true;
    }

    int CBridgeServiceNode::websocket_init()
    {
        websocket_base.registerCallback(std::bind(&CBridgeServiceNode::websocket_message, this,
                                                  std::placeholders::_1, std::placeholders::_2));
        std::thread websocket_run_handle(std::bind(&CWebSocketBase::run, &websocket_base, 9002));
        websocket_run_handle.detach();
        std::thread websocket_message_handle(std::bind(&CBridgeServiceNode::websocket_message_thread, this, nullptr));
        websocket_message_handle.detach();

        return 0;
    }

    void CBridgeServiceNode::websocket_message(websocketpp::connection_hdl hdl, server::message_ptr msg)
    {
        msg_mtx.lock();

        message_t message;
        message.hdl = hdl;
        message.msg = msg;
        message_list.push(message);
        while (message_list.size() > 1000)
            message_list.pop();

        msg_mtx.unlock();

        // std::string msg_str = "{\"code\":0,\"type\":\"confirm\",\"message\":\"success\",\"data\":\"\"}";
        // websocket_base.send(hdl, msg_str, websocketpp::frame::opcode::value::text);
    }

    void CBridgeServiceNode::websocket_message_thread(void *p)
    {
        sleep(1);
        ros::Rate loop_rate(1000);
        while (ros::ok())
        {
            if (message_list.size() > 0)
            {
                msg_mtx.lock();

                message_t message = message_list.front();
                message_list.pop();

                std::string msg = message.msg->get_payload();
                try
                {
                    Json::Reader reader;
                    Json::Value root;
                    if (reader.parse(msg, root))
                    {
                        std::string cmd = root["cmd"].asString();
                        if (cmd == "setworkmode")
                        {
                            std::string mode = root["mode"].asString();
                        }
                        if (cmd == "setcameradata")
                        {
                            std::string visiable = root["visiable"].asString();
                            std::string infrared = root["infrared"].asString();
                            std::string temperature = root["temperature"].asString();
                        }
                        if (cmd == "radardata")
                        {
                            std::string count = root["count"].asString();
                            std::string PTZ = root["PTZ"].asString();
                        }
                        if (cmd == "status")
                        {
                            std::string sensor = root["sensor"].asString();
                        }
                        if (cmd == "workmode")
                        {
                            boost::shared_ptr<blk_control_msgs::WorkMode const> work_mode_sub;
                            work_mode_sub = ros::topic::waitForMessage<blk_control_msgs::WorkMode>(node_options.work_mode, ros::Duration(3.0));
                            if (work_mode_sub != nullptr)
                            {
                                Json::Value json_msg;
                                json_msg["datatype"] = "workmode";
                                json_msg["value"] = work_mode_sub->mode;
                                std::string msg_str = "";
                                msg_str = json_msg.toStyledString();
                                websocket_base.send(message.hdl, msg_str, websocketpp::frame::opcode::value::text);
                            }
                        }
                        if (cmd == "clearalarm")
                        {
                            //
                        }
                        if (cmd == "PTZ")
                        {
                            boost::shared_ptr<nav_msgs::Odometry const> ptz_pose_sub;
                            ptz_pose_sub = ros::topic::waitForMessage<nav_msgs::Odometry>(node_options.ptz_pose, ros::Duration(3.0));
                            if (ptz_pose_sub != nullptr)
                            {
                                Json::Value json_msg;
                                json_msg["datatype"] = "PTZ";
                                json_msg["PTZ"] = std::to_string(ptz_pose_sub->pose.pose.position.x) + "," + std::to_string(ptz_pose_sub->pose.pose.position.z);
                                std::string msg_str = "";
                                msg_str = json_msg.toStyledString();
                                websocket_base.send(message.hdl, msg_str, websocketpp::frame::opcode::value::text);
                            }
                        }
                        // std::string msg_str = "{\"code\":0,\"type\":\"reply\",\"message\":\"success\",\"data\":\"\"}";
                        // websocket_base.send(message.hdl, msg_str, websocketpp::frame::opcode::value::text);
                    }
                }
                catch (const std::exception &e)
                {
                    std::cerr << e.what() << '\n';
                }

                msg_mtx.unlock();
            }

            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    //
    int CBridgeServiceNode::msgRosToMqtt(const std_msgs::String &ros_msg, std::string &mqtt_msg)
    {
        int nRet = 0;

        return nRet;
    }

    int CBridgeServiceNode::msgMqttToRos(const std::string &mqtt_msg, std_msgs::String &ros_msg)
    {
        int nRet = 0;

        return nRet;
    }
}
