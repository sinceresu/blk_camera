#include "blk_bridge_service.hpp"

namespace blk_bridge_service
{
    CBridgeService::CBridgeService()
    {
    }

    CBridgeService::~CBridgeService()
    {
    }

    bool CBridgeService::zipBuff(const char *_buff, int _len, char **_outBuff, int &_outLen)
    {
        _outLen = 0;
        *_outBuff = NULL;
        if (_buff == NULL || _len == 0)
        {
            return false;
        }
        unsigned int maxOutLen = LZ4_compressBound(_len);
        *_outBuff = new char[maxOutLen];
        unsigned int zipBufSize = LZ4_compress_fast(_buff, *_outBuff, _len, maxOutLen, 3);
        if (zipBufSize <= 0)
        {
            delete[](*_outBuff);
            *_outBuff = NULL;
            return false;
        }
        _outLen = zipBufSize;

        return true;
    }

    bool CBridgeService::unZipBuff(const char *_buff, int _len, char *_outBuff, int _outLen)
    {
        // int decompressSize = LZ4_decompress_safe(_buff, _outBuff, _len, _outLen);
        // //int decompressSize = LZ4_decompress_fast( _buff, _len, _outBuff, _outLen);
        // return decompressSize == _outLen;

        return true;
    }

}
