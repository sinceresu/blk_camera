#include "data_sync_node.hpp"

namespace data_sync
{

    CDataSyncNode::CDataSyncNode(NodeOptions _node_options)
        : node_options(_node_options), b_ptz_visiable(false), b_ptz_infrared(false), b_ptz_temperature(false), b_ptz_radar(false)
    {
        LaunchPublishers();
        LaunchSubscribers();
        LaunchService();
        LaunchClinet();
        LaunchSynchronizer();
        LaunchActions();

        std::thread update_thread(std::bind(&CDataSyncNode::update_data, this, nullptr));
        update_thread.detach();
    }

    CDataSyncNode::~CDataSyncNode()
    {
        /* code */
    }

    void CDataSyncNode::LaunchPublishers()
    {
        ptz_visiable_publisher = nh.advertise<data_sync_msgs::VisiablePTZ>(node_options.ptz_visiable, 1);
        ptz_infrared_publisher = nh.advertise<data_sync_msgs::InfraredPTZ>(node_options.ptz_infrared, 1);
        ptz_temperature_publisher = nh.advertise<data_sync_msgs::TemperaturePTZ>(node_options.ptz_temperature, 1);
        ptz_radar_publisher = nh.advertise<data_sync_msgs::RadarPTZ>(node_options.ptz_radar, 1);
    }

    void CDataSyncNode::LaunchSubscribers()
    {
        test_subscriber = nh.subscribe("/data/sync/test", 1, &CDataSyncNode::test_Callback, this);
    }

    void CDataSyncNode::LaunchService()
    {
        ptz_visiable_status_service = nh.advertiseService(node_options.ptz_visiable_status, &CDataSyncNode::ptz_visiable_status_Callback, this);
        ptz_infrared_status_service = nh.advertiseService(node_options.ptz_infrared_status, &CDataSyncNode::ptz_infrared_status_Callback, this);
        ptz_temperature_status_service = nh.advertiseService(node_options.ptz_temperature_status, &CDataSyncNode::ptz_temperature_status_Callback, this);
        ptz_radar_status_service = nh.advertiseService(node_options.ptz_radar_status, &CDataSyncNode::ptz_radar_status_Callback, this);
    }

    void CDataSyncNode::LaunchClinet()
    {
    }

    void CDataSyncNode::LaunchSynchronizer()
    {
        ptz_sync_sub_ = nullptr;
        visiable_sync_sub_ = nullptr;
        infrared_sync_sub_ = nullptr;
        temperature_sync_sub_ = nullptr;
        radar_sync_sub_ = nullptr;
        ptz_visiable_sync_ = nullptr;
        ptz_infrared_sync_ = nullptr;
        ptz_temperature_sync_ = nullptr;
        ptz_radar_sync_ = nullptr;
        ptz_sync_sub_ = new message_filters::Subscriber<nav_msgs::Odometry>(nh, node_options.ptz_pose, 1);
        visiable_sync_sub_ = new message_filters::Subscriber<sensor_msgs::Image>(nh, node_options.visiable_image, 1);
        infrared_sync_sub_ = new message_filters::Subscriber<sensor_msgs::Image>(nh, node_options.infrared_image, 1);
        temperature_sync_sub_ = new message_filters::Subscriber<sensor_msgs::Image>(nh, node_options.infrared_temperature, 1);
        radar_sync_sub_ = new message_filters::Subscriber<sensor_msgs::Range>(nh, node_options.radar_data, 1);
        // ApproximateTime takes a queue size as its constructor argument, hence ptz_visiable_SyncPolicy(1)
        ptz_visiable_sync_ = new message_filters::Synchronizer<ptz_visiable_SyncPolicy>(ptz_visiable_SyncPolicy(1), *ptz_sync_sub_, *visiable_sync_sub_);
        ptz_visiable_sync_->registerCallback(boost::bind(&CDataSyncNode::ptz_visiable_SyncCallback, this, _1, _2));
        // ApproximateTime takes a queue size as its constructor argument, hence ptz_infrared_SyncPolicy(1)
        ptz_infrared_sync_ = new message_filters::Synchronizer<ptz_infrared_SyncPolicy>(ptz_infrared_SyncPolicy(1), *ptz_sync_sub_, *infrared_sync_sub_);
        ptz_infrared_sync_->registerCallback(boost::bind(&CDataSyncNode::ptz_infrared_SyncCallback, this, _1, _2));
        // ApproximateTime takes a queue size as its constructor argument, hence ptz_temperature_SyncPolicy(1)
        ptz_temperature_sync_ = new message_filters::Synchronizer<ptz_temperature_SyncPolicy>(ptz_temperature_SyncPolicy(1), *ptz_sync_sub_, *temperature_sync_sub_);
        ptz_temperature_sync_->registerCallback(boost::bind(&CDataSyncNode::ptz_temperature_SyncCallback, this, _1, _2));
        // ApproximateTime takes a queue size as its constructor argument, hence ptz_radar_SyncPolicy(1)
        ptz_radar_sync_ = new message_filters::Synchronizer<ptz_radar_SyncPolicy>(ptz_radar_SyncPolicy(1), *ptz_sync_sub_, *radar_sync_sub_);
        ptz_radar_sync_->registerCallback(boost::bind(&CDataSyncNode::ptz_radar_SyncCallback, this, _1, _2));
    }

    void CDataSyncNode::LaunchActions()
    {
    }

    void CDataSyncNode::ptz_visiable_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &visiable_msg)
    {
        // ROS_INFO_STREAM("ptz_visiable_SyncCallback ptz_msg:\n"
        //                 << ptz_msg);
        // ROS_INFO_STREAM("ptz_visiable_SyncCallback visiable_msg:\n"
        //                 << visiable_msg);

        data_sync_msgs::VisiablePTZ msg;
        // msg.device_id;
        // msg.camera_id;
        msg.header.stamp = ros::Time::now();
        msg.ptz_pose = *ptz_msg;
        msg.visiable = *visiable_msg;

        visiable_ptz_mtx.lock();
        visiable_ptz_list.push(msg);
        while (visiable_ptz_list.size() > 10)
            visiable_ptz_list.pop();
        visiable_ptz_mtx.unlock();
    }

    void CDataSyncNode::ptz_infrared_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &infrared_msg)
    {
        // ROS_INFO_STREAM("ptz_infrared_SyncCallback ptz_msg:\n"
        //                 << ptz_msg);
        // ROS_INFO_STREAM("ptz_infrared_SyncCallback infrared_msg:\n"
        //                 << infrared_msg);

        data_sync_msgs::InfraredPTZ msg;
        // msg.device_id;
        // msg.camera_id;
        msg.header.stamp = ros::Time::now();
        msg.ptz_pose = *ptz_msg;
        msg.infrared = *infrared_msg;
        infrared_ptz_mtx.lock();
        infrared_ptz_list.push(msg);
        while (infrared_ptz_list.size() > 10)
            infrared_ptz_list.pop();
        infrared_ptz_mtx.unlock();
    }

    void CDataSyncNode::ptz_temperature_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &temperature_msg)
    {
        // ROS_INFO_STREAM("ptz_temperature_SyncCallback ptz_msg:\n"
        //                 << ptz_msg);
        // ROS_INFO_STREAM("ptz_temperature_SyncCallback temperature_msg:\n"
        //                 << temperature_msg);

        data_sync_msgs::TemperaturePTZ msg;
        // msg.device_id;
        // msg.camera_id;
        msg.header.stamp = ros::Time::now();
        msg.ptz_pose = *ptz_msg;
        msg.temperature = *temperature_msg;
        temperature_ptz_mtx.lock();
        temperature_ptz_list.push(msg);
        while (temperature_ptz_list.size() > 10)
            temperature_ptz_list.pop();
        temperature_ptz_mtx.unlock();
    }

    void CDataSyncNode::ptz_radar_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Range::ConstPtr &radar_msg)
    {
        // ROS_INFO_STREAM("ptz_radar_SyncCallback ptz_msg:\n"
        //                 << ptz_msg);
        // ROS_INFO_STREAM("ptz_radar_SyncCallback radar_msg:\n"
        //                 << radar_msg);

        data_sync_msgs::RadarPTZ msg;
        // msg.device_id;
        // msg.camera_id;
        msg.header.stamp = ros::Time::now();
        msg.ptz_pose = *ptz_msg;
        msg.radar = *radar_msg;
        radar_ptz_mtx.lock();
        radar_ptz_list.push(msg);
        while (radar_ptz_list.size() > 10)
            radar_ptz_list.pop();
        radar_ptz_mtx.unlock();
    }

    bool CDataSyncNode::ptz_visiable_status_Callback(data_sync_msgs::VisiableDataStatus::Request &req,
                                                     data_sync_msgs::VisiableDataStatus::Response &res)
    {
        if (req.status == 0)
        {
            b_ptz_visiable = false;
        }
        if (req.status == 1)
        {
            b_ptz_visiable = true;
        }
        if (req.values.size() >= 1)
        {
            num_ptz_visiable = atoi(req.values[0].c_str());
        }

        res.result = 0;
        res.message = "success";

        return true;
    }

    bool CDataSyncNode::ptz_infrared_status_Callback(data_sync_msgs::InfraredDataStatus::Request &req,
                                                     data_sync_msgs::InfraredDataStatus::Response &res)
    {
        if (req.status == 0)
        {
            b_ptz_infrared = false;
        }
        if (req.status == 1)
        {
            b_ptz_infrared = true;
        }
        if (req.values.size() >= 1)
        {
            num_ptz_infrared = atoi(req.values[0].c_str());
        }

        res.result = 0;
        res.message = "success";

        return true;
    }

    bool CDataSyncNode::ptz_temperature_status_Callback(data_sync_msgs::TemperatureDataStatus::Request &req,
                                                        data_sync_msgs::TemperatureDataStatus::Response &res)
    {
        if (req.status == 0)
        {
            b_ptz_temperature = false;
        }
        if (req.status == 1)
        {
            b_ptz_temperature = true;
        }
        if (req.values.size() >= 1)
        {
            num_ptz_temperature = atoi(req.values[0].c_str());
        }

        res.result = 0;
        res.message = "success";

        return true;
    }

    bool CDataSyncNode::ptz_radar_status_Callback(data_sync_msgs::RadarDataStatus::Request &req,
                                                  data_sync_msgs::RadarDataStatus::Response &res)
    {
        if (req.status == 0)
        {
            b_ptz_radar = false;
        }
        if (req.status == 1)
        {
            b_ptz_radar = true;
        }
        if (req.values.size() >= 1)
        {
            num_ptz_radar = atoi(req.values[0].c_str());
        }

        res.result = 0;
        res.message = "success";

        return true;
    }

    void CDataSyncNode::publisher_ptz_visiable()
    {
        if (visiable_ptz_list.size() > 0)
        {
            visiable_ptz_mtx.lock();
            data_sync_msgs::VisiablePTZ msg = visiable_ptz_list.front();
            visiable_ptz_list.pop();
            visiable_ptz_mtx.unlock();
            if (b_ptz_visiable)
            {
                ptz_visiable_publisher.publish(msg);
            }
            else
            {
                if (num_ptz_visiable > 0)
                {
                    num_ptz_visiable--;
                    ptz_visiable_publisher.publish(msg);
                }
            }
        }
    }

    void CDataSyncNode::publisher_ptz_infrared()
    {
        if (infrared_ptz_list.size() > 0)
        {
            infrared_ptz_mtx.lock();
            data_sync_msgs::InfraredPTZ msg = infrared_ptz_list.front();
            infrared_ptz_list.pop();
            infrared_ptz_mtx.unlock();
            if (b_ptz_infrared)
            {
                ptz_infrared_publisher.publish(msg);
            }
            else
            {
                if (num_ptz_infrared > 0)
                {
                    num_ptz_infrared--;
                    ptz_infrared_publisher.publish(msg);
                }
            }
        }
    }

    void CDataSyncNode::publisher_ptz_temperature()
    {
        if (temperature_ptz_list.size() > 0)
        {
            temperature_ptz_mtx.lock();
            data_sync_msgs::TemperaturePTZ msg = temperature_ptz_list.front();
            temperature_ptz_list.pop();
            temperature_ptz_mtx.unlock();
            if (b_ptz_temperature)
            {
                ptz_temperature_publisher.publish(msg);
            }
            else
            {
                if (num_ptz_temperature > 0)
                {
                    num_ptz_temperature--;
                    ptz_temperature_publisher.publish(msg);
                }
            }
        }
    }

    void CDataSyncNode::publisher_ptz_radar()
    {
        if (radar_ptz_list.size() > 0)
        {
            radar_ptz_mtx.lock();
            data_sync_msgs::RadarPTZ msg = radar_ptz_list.front();
            radar_ptz_list.pop();
            radar_ptz_mtx.unlock();
            if (b_ptz_radar)
            {
                ptz_radar_publisher.publish(msg);
            }
            else
            {
                if (num_ptz_radar > 0)
                {
                    num_ptz_radar--;
                    ptz_radar_publisher.publish(msg);
                }
            }
        }
    }

    bool b_save = false;
    void CDataSyncNode::test_Callback(const std_msgs::String::ConstPtr &msg)
    {
        b_save = true;
    }

    void CDataSyncNode::update_data(void *p)
    {
        // std::vector<unsigned int> items{1, 2, 3};
        int index = 0;
        // ros::Rate loop_rate(0.6);
        ros::Rate loop_rate(1);
        while (ros::ok())
        {
            //
            publisher_ptz_visiable();
            publisher_ptz_infrared();
            publisher_ptz_temperature();
            publisher_ptz_radar();
            ros::spinOnce();
            loop_rate.sleep();
            continue;
            //

            if (index == 0)
            {
                if (visiable_ptz_list.size() > 0)
                {
                    visiable_ptz_mtx.lock();
                    data_sync_msgs::VisiablePTZ msg = visiable_ptz_list.front();
                    visiable_ptz_list.pop();
                    visiable_ptz_mtx.unlock();
                    if (b_ptz_visiable)
                        ptz_visiable_publisher.publish(msg);
                    if (b_save)
                    {
                        b_save = false;
                        std::string srcFile = std::to_string(msg.ptz_pose.pose.pose.position.x) + "-" +
                                              std::to_string(msg.ptz_pose.pose.pose.position.z) + "-img.jpg";
                        std::cout << "[update_data] srcFile:" << srcFile << std::endl;
                        cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg.visiable, sensor_msgs::image_encodings::TYPE_8UC3);
                        cv::Mat img = cv_ptr->image;
                        cv::imwrite("/home/lihao/Workspace/test_data/blk_jpg/" + srcFile, img);
                    }
                }
                index = 1;
            }
            else if (index == 1)
            {
                if (infrared_ptz_list.size() > 0)
                {
                    infrared_ptz_mtx.lock();
                    data_sync_msgs::InfraredPTZ msg = infrared_ptz_list.front();
                    infrared_ptz_list.pop();
                    infrared_ptz_mtx.unlock();
                    if (b_ptz_infrared)
                        ptz_infrared_publisher.publish(msg);
                }
                index = 2;
            }
            else if (index == 2)
            {
                if (temperature_ptz_list.size() > 0)
                {
                    temperature_ptz_mtx.lock();
                    data_sync_msgs::TemperaturePTZ msg = temperature_ptz_list.front();
                    temperature_ptz_list.pop();
                    temperature_ptz_mtx.unlock();
                    if (b_ptz_temperature)
                        ptz_temperature_publisher.publish(msg);
                }
                index = 3;
            }
            else if (index == 3)
            {
                if (radar_ptz_list.size() > 0)
                {
                    radar_ptz_mtx.lock();
                    data_sync_msgs::RadarPTZ msg = radar_ptz_list.front();
                    radar_ptz_list.pop();
                    radar_ptz_mtx.unlock();
                    if (b_ptz_radar)
                        ptz_radar_publisher.publish(msg);
                }
                index = 0;
            }

            ros::spinOnce();
            loop_rate.sleep();
        }
    }

}
