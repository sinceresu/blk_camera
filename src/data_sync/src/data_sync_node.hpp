#ifndef data_sync_node_HPP
#define data_sync_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include <thread>
#include "ros/ros.h"
#include "ros/package.h"
#include "cv_bridge/cv_bridge.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/Range.h"
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include "data_sync_msgs/VisiablePTZ.h"
#include "data_sync_msgs/InfraredPTZ.h"
#include "data_sync_msgs/TemperaturePTZ.h"
#include "data_sync_msgs/RadarPTZ.h"
#include "data_sync_msgs/VisiableDataStatus.h"
#include "data_sync_msgs/InfraredDataStatus.h"
#include "data_sync_msgs/TemperatureDataStatus.h"
#include "data_sync_msgs/RadarDataStatus.h"

#include "http_client.hpp"
#include <opencv2/opencv.hpp>

namespace data_sync
{

    class CDataSyncNode
    {
    public:
        typedef struct _NodeOptions
        {
            int device_id;
            int camera_id;
            //
            std::string ptz_pose;
            std::string visiable_image;
            std::string infrared_image;
            std::string infrared_temperature;
            std::string radar_data;
            //
            std::string ptz_visiable;
            std::string ptz_infrared;
            std::string ptz_temperature;
            std::string ptz_radar;
            //
            std::string ptz_visiable_status;
            std::string ptz_infrared_status;
            std::string ptz_temperature_status;
            std::string ptz_radar_status;
        } NodeOptions;

    public:
        CDataSyncNode(NodeOptions _node_options);
        ~CDataSyncNode();

    private:
        NodeOptions node_options;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::Image> ptz_visiable_SyncPolicy;
        typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::Image> ptz_infrared_SyncPolicy;
        typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::Image> ptz_temperature_SyncPolicy;
        typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::Range> ptz_radar_SyncPolicy;
        message_filters::Subscriber<nav_msgs::Odometry> *ptz_sync_sub_;
        message_filters::Subscriber<sensor_msgs::Image> *visiable_sync_sub_;
        message_filters::Subscriber<sensor_msgs::Image> *infrared_sync_sub_;
        message_filters::Subscriber<sensor_msgs::Image> *temperature_sync_sub_;
        message_filters::Subscriber<sensor_msgs::Range> *radar_sync_sub_;
        message_filters::Synchronizer<ptz_visiable_SyncPolicy> *ptz_visiable_sync_;
        message_filters::Synchronizer<ptz_infrared_SyncPolicy> *ptz_infrared_sync_;
        message_filters::Synchronizer<ptz_temperature_SyncPolicy> *ptz_temperature_sync_;
        message_filters::Synchronizer<ptz_radar_SyncPolicy> *ptz_radar_sync_;
        void ptz_visiable_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &visiable_msg);
        void ptz_infrared_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &infrared_msg);
        void ptz_temperature_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Image::ConstPtr &temperature_msg);
        void ptz_radar_SyncCallback(const nav_msgs::Odometry::ConstPtr &ptz_msg, const sensor_msgs::Range::ConstPtr &radar_msg);

        ros::Publisher ptz_visiable_publisher;
        void publisher_ptz_visiable();
        ros::Publisher ptz_infrared_publisher;
        void publisher_ptz_infrared();
        ros::Publisher ptz_temperature_publisher;
        void publisher_ptz_temperature();
        ros::Publisher ptz_radar_publisher;
        void publisher_ptz_radar();

        ros::Subscriber test_subscriber;
        void test_Callback(const std_msgs::String::ConstPtr &msg);

        ros::ServiceServer ptz_visiable_status_service;
        bool ptz_visiable_status_Callback(data_sync_msgs::VisiableDataStatus::Request &req,
                                          data_sync_msgs::VisiableDataStatus::Response &res);
        ros::ServiceServer ptz_infrared_status_service;
        bool ptz_infrared_status_Callback(data_sync_msgs::InfraredDataStatus::Request &req,
                                          data_sync_msgs::InfraredDataStatus::Response &res);
        ros::ServiceServer ptz_temperature_status_service;
        bool ptz_temperature_status_Callback(data_sync_msgs::TemperatureDataStatus::Request &req,
                                             data_sync_msgs::TemperatureDataStatus::Response &res);
        ros::ServiceServer ptz_radar_status_service;
        bool ptz_radar_status_Callback(data_sync_msgs::RadarDataStatus::Request &req,
                                       data_sync_msgs::RadarDataStatus::Response &res);

        void update_data(void *p);

        std::mutex visiable_ptz_mtx;
        std::queue<data_sync_msgs::VisiablePTZ> visiable_ptz_list;
        std::mutex infrared_ptz_mtx;
        std::queue<data_sync_msgs::InfraredPTZ> infrared_ptz_list;
        std::mutex temperature_ptz_mtx;
        std::queue<data_sync_msgs::TemperaturePTZ> temperature_ptz_list;
        std::mutex radar_ptz_mtx;
        std::queue<data_sync_msgs::RadarPTZ> radar_ptz_list;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchClinet();
        void LaunchSynchronizer();
        void LaunchActions();

    private:
        bool b_ptz_visiable;
        int num_ptz_visiable{0};
        bool b_ptz_infrared;
        int num_ptz_infrared{0};
        bool b_ptz_temperature;
        int num_ptz_temperature{0};
        bool b_ptz_radar;
        int num_ptz_radar{0};
    };

}

#endif