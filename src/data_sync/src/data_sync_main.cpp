#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "data_sync_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace data_sync
{

    void run()
    {
        CDataSyncNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);
        ros::param::get("~camera_id", node_options.camera_id);
        //
        ros::param::get("~ptz_pose", node_options.ptz_pose);
        ros::param::get("~visiable_image", node_options.visiable_image);
        ros::param::get("~infrared_image", node_options.infrared_image);
        ros::param::get("~infrared_temperature", node_options.infrared_temperature);
        ros::param::get("~radar_data", node_options.radar_data);
        //
        ros::param::get("~ptz_visiable", node_options.ptz_visiable);
        ros::param::get("~ptz_infrared", node_options.ptz_infrared);
        ros::param::get("~ptz_temperature", node_options.ptz_temperature);
        ros::param::get("~ptz_radar", node_options.ptz_radar);
        //
        ros::param::get("~ptz_visiable_status", node_options.ptz_visiable_status);
        ros::param::get("~ptz_infrared_status", node_options.ptz_infrared_status);
        ros::param::get("~ptz_temperature_status", node_options.ptz_temperature_status);
        ros::param::get("~ptz_radar_status", node_options.ptz_radar_status);

        CDataSyncNode data_sync_node(node_options);

        ROS_INFO("data_sync node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }

}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("data_sync");

    ros::init(argc, argv, "data_sync");
    ros::Time::init();

    data_sync::run();

    ros::shutdown();

    return 0;
}