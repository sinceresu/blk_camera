
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <time.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/String.h"
#include "ros/ros.h"
#include "ptocreator/start.h"
#include "ptocreator/stop.h"
#include "ptocreator/collect.h"
using namespace std;
std::string output_pto_path;
int pano_w,pano_h,w,h,sum;
float v;
ofstream fout;
void getParameters()
{
    cout << "Get the parameters from the launch file" << endl;
    if (!ros::param::get("output_pto_path", output_pto_path))
    {
        cout << "Can not get the value of output_pto_path" << endl;
        exit(1);
    }
    if (!ros::param::get("pano_w", pano_w))
    {
        cout << "Can not get the value of pano_w" << endl;
        exit(1);
    }
    if (!ros::param::get("pano_h", pano_h))
    {
        cout << "Can not get the value of pano_h" << endl;
        exit(1);
    }
    if (!ros::param::get("w", w))
    {
        cout << "Can not get the value of w" << endl;
        exit(1);
    }
    if (!ros::param::get("h", h))
    {
        cout << "Can not get the value of h" << endl;
        exit(1);
    }
    if (!ros::param::get("v", v))
    {
        cout << "Can not get the value of v" << endl;
        exit(1);
    }
}
bool startcapture(ptocreator::start::Request &req,ptocreator::start::Response &res)
{
    ROS_INFO("startcapture recieve request");
    fout.open(output_pto_path);
    sum=0;
    std::cout
            <<"sum is "<<req.sum<<std::endl;
    sum=req.sum;
    fout << "# hugin project file"<< endl
         <<"#hugin_ptoversion 2"<< endl
         <<"p f2 w"<<pano_w<<" h"<<pano_h<<" v360  k0 E0 R0 n\"TIFF_m c:LZW r:CROP\""<<endl
         << "m i0"<<endl<<endl
         << "# image lines"<< endl;
    return true;
}
bool collectpose(ptocreator::collect::Request &req,ptocreator::collect::Response &res){
    ROS_INFO("collectpose recieve request");
    std::cout
            <<"roll_angle is "<<req.roll_angle<<std::endl
            <<"pitch_angle is "<<req.pitch_angle<<std::endl
            <<"yaw_angle is "<<req.yaw_angle<<std::endl
            <<"imgname is "<<req.imgname<<std::endl;
    float roll=req.roll_angle;
    float pitch=req.pitch_angle;
    float yaw=req.yaw_angle;
    std::string imgname=req.imgname;
    fout << "#-hugin  cropFactor=1"<< endl
         <<"i w"<<w<<" h"<<h<<" f0 v"<<v<<" Ra0 Rb0 Rc0 Rd0 Re0 Eev0 Er1 Eb1 r"<<roll<<" p"<<pitch<<" y"<<yaw
         <<" TrX0 TrY0 TrZ0 Tpy0 Tpp0 j0 a0 b0 c0 d0 e0 g0 t0 Va1 Vb0 Vc0 Vd0 Vx0 Vy0  Vm5 n"<<"\""<<imgname<<"\""<<endl;

    return true;

}
bool stopcapture(ptocreator::stop::Request &req,ptocreator::stop::Response &res)
{
    ROS_INFO("stopcapture recieve request");
    fout<<endl<<endl<<"# specify variables that should be optimized"<<endl;
    fout<<"v Ra0"<<endl<<"v Rb0"<<endl<<"v Rc0"<<endl<<"v Rd0"<<endl<<"v Re0"<<endl<<"v Vb0"<<endl<<"v Vc0"<<endl<<"v Vd0"<<endl;
    for(int i=1;i<sum+1;i++)
    {
        fout<<"v Eev"<<i<<endl<<"v r"<<i<<endl<<"v p"<<i<<endl<<"v y"<<i<<endl;
    }
    fout<<endl<<endl<<"# control points"<<endl;
    fout<<"#hugin_optimizeReferenceImage 0\n"
          "#hugin_blender enblend\n"
          "#hugin_remapper nona\n"
          "#hugin_enblendOptions \n"
          "#hugin_enfuseOptions \n"
          "#hugin_hdrmergeOptions -m avg -c\n"
          "#hugin_verdandiOptions \n"
          "#hugin_outputLDRBlended true\n"
          "#hugin_outputLDRLayers false\n"
          "#hugin_outputLDRExposureRemapped false\n"
          "#hugin_outputLDRExposureLayers false\n"
          "#hugin_outputLDRExposureBlended false\n"
          "#hugin_outputLDRStacks false\n"
          "#hugin_outputLDRExposureLayersFused false\n"
          "#hugin_outputHDRBlended false\n"
          "#hugin_outputHDRLayers false\n"
          "#hugin_outputHDRStacks false\n"
          "#hugin_outputLayersCompression LZW\n"
          "#hugin_outputImageType tif\n"
          "#hugin_outputImageTypeCompression LZW\n"
          "#hugin_outputJPEGQuality 90\n"
          "#hugin_outputImageTypeHDR exr\n"
          "#hugin_outputImageTypeHDRCompression LZW\n"
          "#hugin_outputStacksMinOverlap 0.7\n"
          "#hugin_outputLayersExposureDiff 0.5\n"
          "#hugin_outputRangeCompression 0\n"
          "#hugin_optimizerMasterSwitch 1\n"
          "#hugin_optimizerPhotoMasterSwitch 21"<<endl;
    fout.close();
    return true;
}
int main(int argc, char **argv)
{
    ROS_INFO("start ptocreator");
    ros::init(argc, argv, "ptocreator");
    ros::NodeHandle nh;
    getParameters();
    ros::ServiceServer star_capture = nh.advertiseService("/bluelight/inspect/ircam_pose_start",  &startcapture);
    ros::ServiceServer collect_capture = nh.advertiseService("/bluelight/inspect/ircam_pose_collect",  &collectpose);
    ros::ServiceServer stop_capture = nh.advertiseService("/bluelight/inspect/ircam_pose_stop",  &stopcapture);
    ros::spin();
    return 0;
}
