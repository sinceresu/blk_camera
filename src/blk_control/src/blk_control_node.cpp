#include "blk_control_node.hpp"

namespace blk_control
{

    CBLKControlNode::CBLKControlNode(NodeOptions _node_options)
        : node_options(_node_options)
    {
        work_mode = "circle";

        LaunchPublishers();
        LaunchSubscribers();
        LaunchService();
        LaunchClinet();
        LaunchActions();

        std::thread update_thread(std::bind(&CBLKControlNode::update_data, this, nullptr));
        update_thread.detach();
    }

    CBLKControlNode::~CBLKControlNode()
    {
        /* code */
    }

    void CBLKControlNode::LaunchPublishers()
    {
        work_mode_publisher = nh.advertise<blk_control_msgs::WorkMode>(node_options.work_mode, 1);
    }

    void CBLKControlNode::LaunchSubscribers()
    {
        ptz_pose_subscriber = nh.subscribe(node_options.ptz_pose, 1, &CBLKControlNode::ptz_pose_Callback, this);
        ptz_pose_isreach_subscriber = nh.subscribe(node_options.ptz_pose_isreach, 1, &CBLKControlNode::ptz_pose_isreach_Callback, this);
    }

    void CBLKControlNode::LaunchService()
    {
        task_data_service = nh.advertiseService(node_options.task_data, &CBLKControlNode::task_data_service_Callback, this);
        task_control_service = nh.advertiseService(node_options.task_control, &CBLKControlNode::task_control_service_Callback, this);
        set_work_mode_service = nh.advertiseService(node_options.set_work_mode, &CBLKControlNode::set_work_mode_service_Callback, this);
        get_work_mode_service = nh.advertiseService(node_options.get_work_mode, &CBLKControlNode::get_work_mode_service_Callback, this);
        get_ptz_pose_service = nh.advertiseService(node_options.get_ptz_pose, &CBLKControlNode::get_ptz_pose_service_Callback, this);
        get_radar_data_service = nh.advertiseService(node_options.get_radar_data, &CBLKControlNode::get_radar_data_service_Callback, this);
    }

    void CBLKControlNode::LaunchClinet()
    {
        ptz_pose_client = nh.serviceClient<fixed_msg::cp_control>(node_options.call_ptz_pose);
    }

    void CBLKControlNode::LaunchActions()
    {
    }

    void CBLKControlNode::LaunchTime()
    {
        // ros::Timer timer = nh.createTimer(ros::Duration(0.1), &CBLKControlNode::tick, this, false);
    }

    void CBLKControlNode::publisher_work_mode(int _device_id, int _camera_id, std::string _mode)
    {
        blk_control_msgs::WorkMode work_mode;
        work_mode.device_id = _device_id;
        work_mode.camera_id = _camera_id;
        work_mode.mode = _mode;
        work_mode_publisher.publish(work_mode);
    }

    void CBLKControlNode::publisher_work_state(int _device_id, int _camera_id, std::string _state)
    {
    }

    void CBLKControlNode::ptz_pose_Callback(const nav_msgs::Odometry::ConstPtr &msg)
    {
        ptz_pose_list.push(*msg);
        while (ptz_pose_list.size() > 1)
        {
            ptz_pose_list.pop();
        }
    }

    void CBLKControlNode::ptz_pose_isreach_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
    }

    bool CBLKControlNode::task_data_service_Callback(blk_control_msgs::TaskData::Request &req,
                                                     blk_control_msgs::TaskData::Response &res)
    {
        ROS_INFO_STREAM("task_data_service_Callback req:\n"
                        << req);

        // test
        if (blk_control.add_task(req.data) == 0)
        {
            blk_control.con_task(0);
        }
        // --

        res.result = 0;
        res.message = "success";

        return true;
    }

    bool CBLKControlNode::task_control_service_Callback(blk_control_msgs::TaskControl::Request &req,
                                                        blk_control_msgs::TaskControl::Response &res)
    {
        ROS_INFO_STREAM("task_control_service_Callback req:\n"
                        << req);

        res.result = 0;
        res.message = "success";

        return true;
    }

    bool CBLKControlNode::set_work_mode_service_Callback(blk_control_msgs::SetWorkMode::Request &req,
                                                         blk_control_msgs::SetWorkMode::Response &res)
    {
        ROS_INFO_STREAM("set_work_mode_service_Callback req:\n"
                        << req);

        work_mode = req.mode;

        res.result = 0;
        res.message = "success";

        return true;
    }

    bool CBLKControlNode::get_work_mode_service_Callback(blk_control_msgs::GetWorkMode::Request &req,
                                                         blk_control_msgs::GetWorkMode::Response &res)
    {
        ROS_INFO_STREAM("get_work_mode_service_Callback req:\n"
                        << req);

        res.result = 0;
        res.message = "success";
        res.mode = work_mode;

        return true;
    }

    bool CBLKControlNode::get_ptz_pose_service_Callback(blk_control_msgs::GetPTZPose::Request &req,
                                                        blk_control_msgs::GetPTZPose::Response &res)
    {
        ROS_INFO_STREAM("get_ptz_pose_service_Callback req:\n"
                        << req);

        boost::shared_ptr<nav_msgs::Odometry const> ptz_pose_sub;
        ptz_pose_sub = ros::topic::waitForMessage<nav_msgs::Odometry>(node_options.ptz_pose, ros::Duration(3.0));
        if (ptz_pose_sub != nullptr)
        {
            res.result = 0;
            res.message = "success";
            res.pose = *ptz_pose_sub;
        }
        else
        {
            res.result = -1;
            res.message = "get ptz pose fail";
            // res.pose;
        }

        return true;
    }

    bool CBLKControlNode::get_radar_data_service_Callback(blk_control_msgs::GetRadarData::Request &req,
                                                          blk_control_msgs::GetRadarData::Response &res)
    {
        ROS_INFO_STREAM("get_radar_data_service_Callback req:\n"
                        << req);

        boost::shared_ptr<sensor_msgs::Range const> radar_data_sub;
        radar_data_sub = ros::topic::waitForMessage<sensor_msgs::Range>(node_options.radar_data, ros::Duration(3.0));
        if (radar_data_sub != nullptr)
        {
            res.result = 0;
            res.message = "success";
            res.range = *radar_data_sub;
        }
        else
        {
            res.result = -1;
            res.message = "get radar data fail";
            // res.range;
        }

        return true;
    }

    bool CBLKControlNode::call_ptz_pose_service(int _device_id, int _camera_id, int _id, int _action, int _type,
                                                double _value, double _x, double _y, double _z)
    {
        bool bRet = false;
        fixed_msg::cp_control msg;
        msg.request.device_id = node_options.device_id;
        msg.request.id = _id;
        msg.request.action = _action;
        msg.request.type = _type;
        msg.request.value = _value;
        msg.request.allvalue.push_back(_x);
        msg.request.allvalue.push_back(_y);
        msg.request.allvalue.push_back(_z);
        if (ptz_pose_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_pose_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_pose_service failed");
        }

        return bRet;
    }

    void CBLKControlNode::tick(const ros::TimerEvent &event)
    {
        // publisher_work_mode(node_options.device_id, node_options.camera_id, work_mode);
    }

    void CBLKControlNode::update_data(void *p)
    {
        ros::Rate loop_rate(1);
        while (ros::ok())
        {
            publisher_work_mode(node_options.device_id, node_options.camera_id, work_mode);
            publisher_work_state(node_options.device_id, node_options.camera_id, work_state);

            ros::spinOnce();
            loop_rate.sleep();
        }
    }

}