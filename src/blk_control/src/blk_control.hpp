#ifndef blk_control_HPP
#define blk_control_HPP

#pragma once
#include <queue>
#include <mutex>
#include <thread>
#include <opencv2/opencv.hpp>
#include <jsoncpp/json/json.h>
#include "http_client.hpp"
#include "task/task_control.hpp"

namespace blk_control
{

    typedef struct _task_t
    {
        std::string taskData;
        int type;
        std::string rule;
    } task_t;

    class CBLKControl
    {
    public:
        CBLKControl();
        ~CBLKControl();

    private:
        std::unique_ptr<CTaskControl> task_control_ptr;
        void init_task_controller();

    public:
        int add_task(std::string _data);
        int con_task(int _status);
        void task_loop(void *p);
    };

}

#endif