#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "blk_control_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace blk_control
{

    void run()
    {
        CBLKControlNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);
        ros::param::get("~camera_id", node_options.camera_id);
        //
        ros::param::get("~task_data", node_options.task_data);
        ros::param::get("~task_control", node_options.task_control);
        ros::param::get("~work_mode", node_options.work_mode);
        ros::param::get("~ptz_pose", node_options.ptz_pose);
        ros::param::get("~ptz_pose_isreach", node_options.ptz_pose_isreach);
        ros::param::get("~radar_data", node_options.radar_data);
        ros::param::get("~set_work_mode", node_options.set_work_mode);
        ros::param::get("~get_work_mode", node_options.get_work_mode);
        ros::param::get("~call_ptz_pose", node_options.call_ptz_pose);
        ros::param::get("~get_ptz_pose", node_options.get_ptz_pose);
        ros::param::get("~get_radar_data", node_options.get_radar_data);

        CBLKControlNode blk_control_node(node_options);

        ROS_INFO("blk_control node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }

}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("blk_control");

    ros::init(argc, argv, "blk_control");
    ros::Time::init();

    blk_control::run();

    ros::shutdown();

    return 0;
}