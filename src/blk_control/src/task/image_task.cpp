#include "image_task.hpp"

namespace blk_control
{

    CImageTask::CImageTask()
        : b_init(false), CTaskBase("image", EMode::Image)
    {
    }

    CImageTask::~CImageTask()
    {
    }

    void CImageTask::load_param()
    {
        ros::param::get("~device_id", image_options.device_id);
        //
        ros::param::get("~call_ptz_pose", image_options.call_ptz_pose);
        ros::param::get("~ptz_pose_isreach", image_options.ptz_pose_isreach);
        ros::param::get("~ptz_visiable", image_options.ptz_visiable);
        ros::param::get("~ptz_infrared", image_options.ptz_infrared);
        ros::param::get("~ptz_visiable_status", image_options.ptz_visiable_status);
        ros::param::get("~ptz_infrared_status", image_options.ptz_infrared_status);
    }

    void CImageTask::LaunchPublishers()
    {
    }

    void CImageTask::LaunchSubscribers()
    {
        ptz_pose_isreach_subscriber = nh.subscribe(image_options.ptz_pose_isreach, 1, &CImageTask::ptz_pose_isreach_Callback, this);
        ptz_visiable_Subscriber = nh.subscribe(image_options.ptz_visiable, 1, &CImageTask::ptz_visiable_Callback, this);
        ptz_infrared_Subscriber = nh.subscribe(image_options.ptz_infrared, 1, &CImageTask::ptz_infrared_Callback, this);
    }

    void CImageTask::LaunchService()
    {
    }

    void CImageTask::LaunchClinet()
    {
        ptz_pose_client = nh.serviceClient<fixed_msg::cp_control>(image_options.call_ptz_pose);
        ptz_visiable_status_client = nh.serviceClient<data_sync_msgs::InfraredDataStatus>(image_options.ptz_visiable_status);
        ptz_infrared_status_client = nh.serviceClient<data_sync_msgs::RadarDataStatus>(image_options.ptz_infrared_status);
    }

    void CImageTask::LaunchActions()
    {
    }

    void CImageTask::LaunchTime()
    {
    }

    void CImageTask::ptz_pose_isreach_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        if (image_item_info_ptr && image_item_info_ptr->step() == EImageItemStep::PTZ_Action_Motion)
        {
            if (msg->data == 1)
            {
                image_item_info_ptr->set(EImageItemStep::PTZ_Action_Done);
            }
        }
    }

    void CImageTask::ptz_visiable_Callback(const data_sync_msgs::VisiablePTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (image_task_ptr && image_item_info_ptr && image_item_info_ptr->step() == EImageItemStep::Collect_Visiable_Wait)
        {
            // std::string srcFile = std::to_string(GetTimeStamp()) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-img.jpg";

            // std::string srcFile = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-img.jpg";
            image_target_t &target = image_task_ptr->targets[image_item_info_ptr->index()];
            std::vector<std::string> pose;
            SplitString(target.pose, ",", pose);
            float pose_x = atof(pose[0].c_str());
            float pose_y = atof(pose[1].c_str());
            float pose_z = atof(pose[2].c_str());
            std::string srcFile = std::to_string(pose_x) + "-" + std::to_string(pose_y) + "-visiable_img.jpg";
            std::cout << "[ptz_visiable_Callback] srcFile:" << srcFile << std::endl;
            cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(_msg->visiable, sensor_msgs::image_encodings::TYPE_8UC3);
            cv::Mat img = cv_ptr->image;
            cv::imwrite("/home/lh/workspace/test_data/blk_img/" + srcFile, img);

            image_item_info_ptr->set(EImageItemStep::Collect_Visiable_Done);
        }
    }

    void CImageTask::ptz_infrared_Callback(const data_sync_msgs::InfraredPTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (image_task_ptr && image_item_info_ptr && image_item_info_ptr->step() == EImageItemStep::Collect_Infrared_Wait)
        {
            // std::string srcFile = std::to_string(GetTimeStamp()) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-img.jpg";

            // std::string srcFile = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-img.jpg";
            image_target_t &target = image_task_ptr->targets[image_item_info_ptr->index()];
            std::vector<std::string> pose;
            SplitString(target.pose, ",", pose);
            float pose_x = atof(pose[0].c_str());
            float pose_y = atof(pose[1].c_str());
            float pose_z = atof(pose[2].c_str());
            std::string srcFile = std::to_string(pose_x) + "-" + std::to_string(pose_y) + "-infrared_img.jpg";
            std::cout << "[ptz_infrared_Callback] srcFile:" << srcFile << std::endl;
            cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(_msg->infrared, sensor_msgs::image_encodings::TYPE_8UC3);
            cv::Mat img = cv_ptr->image;
            cv::imwrite("/home/lh/workspace/test_data/blk_img/" + srcFile, img);

            image_item_info_ptr->set(EImageItemStep::Collect_Infrared_Done);
        }
    }

    bool CImageTask::call_ptz_pose_service(int _device_id, int _id, int _action, int _type,
                                           double _value, double _x, double _y, double _z)
    {
        bool bRet = false;
        fixed_msg::cp_control msg;
        msg.request.device_id = image_options.device_id;
        msg.request.id = _id;
        msg.request.action = _action;
        msg.request.type = _type;
        msg.request.value = _value;
        msg.request.allvalue.push_back(_x);
        msg.request.allvalue.push_back(_y);
        msg.request.allvalue.push_back(_z);
        if (ptz_pose_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_pose_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_pose_service failed");
        }

        return bRet;
    }

    bool CImageTask::call_ptz_visiable_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::VisiableDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_visiable_status_service request:\n"
                        << msg.request);
        if (ptz_visiable_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_visiable_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_visiable_status_service failed");
        }

        return bRet;
    }

    bool CImageTask::call_ptz_infrared_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::InfraredDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_infrared_status_service request:\n"
                        << msg.request);
        if (ptz_infrared_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_infrared_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_infrared_status_service failed");
        }

        return bRet;
    }

    bool CImageTask::timeout_tick(time_t _start_time, int _timeout_ms)
    {
        time_t now_time = GetTimeStamp();
        if ((now_time - _start_time) > _timeout_ms)
        {
            return true;
        }
        return false;
    }

    int CImageTask::task_response(Task_t task, EStatus _status, float progress)
    {
        std::cout << "task " << getLabel() << std::endl;
        std::cout << task.device_id << " "
                  << task.task_execute_id << " "
                  << task.task_id << " "
                  << task.task_name << " "
                  << task.task_type << std::endl;
        return 0;
    }

    int CImageTask::task_progress()
    {
        if (image_task_ptr == nullptr)
            return -1;
        if (image_item_info_ptr == nullptr)
            return -1;

        int item_count = image_task_ptr->targets.size();
        int item_index = image_item_info_ptr->index();
        if (item_count <= 0 || item_index < 0 || item_index >= item_count)
            return -1;

        int progress = item_index * item_count * 0.01;

        return progress;
    }

    int CImageTask::task_execute()
    {
        if (image_task_ptr == nullptr)
            return -1;
        if (image_item_info_ptr == nullptr)
            return -1;

        int item_count = image_task_ptr->targets.size();
        int item_index = image_item_info_ptr->index();
        if (item_count <= 0 || item_index < 0 || item_index >= item_count)
            return -1;

        switch (image_item_info_ptr->step())
        {
        case EImageItemStep::Init:
        {
            image_item_info_ptr->set(EImageItemStep::PTZ_Action_Control);
        }
        break;
        case EImageItemStep::PTZ_Action_Control:
        {
            image_target_t &target = image_task_ptr->targets[item_index];
            std::vector<std::string> pose;
            SplitString(target.pose, ",", pose);
            float pose_x = atof(pose[0].c_str());
            float pose_y = atof(pose[1].c_str());
            float pose_z = atof(pose[2].c_str());
            if (call_ptz_pose_service(image_options.device_id, 1, 1, 3, 0, pose_x, pose_y, pose_z))
            {
                image_item_info_ptr->set(EImageItemStep::PTZ_Action_Motion);
            }
            else
            {
                image_item_info_ptr->set(EImageItemStep::Exception);
            }
        }
        break;
        case EImageItemStep::PTZ_Action_Motion:
        {
            if (timeout_tick(image_item_info_ptr->time(), 30 * 1000))
            {
                image_item_info_ptr->set(EImageItemStep::Timeout);
            }
        }
        break;
        case EImageItemStep::PTZ_Action_Done:
        {
            if (timeout_tick(image_item_info_ptr->time(), 3 * 1000))
            {
                image_target_t &target = image_task_ptr->targets[item_index];
                if (target.target_type == 0)
                {
                    image_item_info_ptr->set(EImageItemStep::Collect_Visiable);
                }
                if (target.target_type == 1)
                {
                    image_item_info_ptr->set(EImageItemStep::Collect_Infrared);
                }
            }
        }
        break;
        case EImageItemStep::Collect_Visiable:
        {
            if (call_ptz_visiable_status_service(image_task_ptr->device_id, 1, 0, 1))
            {
                image_item_info_ptr->set(EImageItemStep::Collect_Visiable_Wait);
            }
            else
            {
                image_item_info_ptr->set(EImageItemStep::Exception);
            }
        }
        break;
        case EImageItemStep::Collect_Visiable_Wait:
        {
            if (timeout_tick(image_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_visiable_status_service(image_task_ptr->device_id, 1, 0, 0);
                image_item_info_ptr->set(EImageItemStep::Timeout);
            }
        }
        break;
        case EImageItemStep::Collect_Visiable_Done:
        {
            call_ptz_visiable_status_service(image_task_ptr->device_id, 1, 0, 0);
            image_item_info_ptr->set(EImageItemStep::Finish);
        }
        break;
        case EImageItemStep::Collect_Infrared:
        {
            if (call_ptz_infrared_status_service(image_task_ptr->device_id, 1, 0, 1))
            {
                image_item_info_ptr->set(EImageItemStep::Collect_Infrared_Wait);
            }
            else
            {
                image_item_info_ptr->set(EImageItemStep::Exception);
            }
        }
        break;
        case EImageItemStep::Collect_Infrared_Wait:
        {
            if (timeout_tick(image_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_infrared_status_service(image_task_ptr->device_id, 1, 0, 0);
                image_item_info_ptr->set(EImageItemStep::Timeout);
            }
        }
        break;
        case EImageItemStep::Collect_Infrared_Done:
        {
            call_ptz_infrared_status_service(image_task_ptr->device_id, 1, 0, 0);
            image_item_info_ptr->set(EImageItemStep::Finish);
        }
        break;
            break;
        case EImageItemStep::Finish:
        case EImageItemStep::Timeout:
        case EImageItemStep::Exception:
        case EImageItemStep::Unknown:
            if (item_index == (item_count - 1))
            {
                status = EStatus::Finish;
            }
            else
            {
                image_item_info_ptr->next();
            }
            break;

        default:
            image_item_info_ptr->next();
            break;
        }

        return 0;
    }

    int CImageTask::load_task(std::string _json)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_json, root))
        {
            return -1;
        }
        std::unique_ptr<image_task_t> task_ptr(new image_task_t());
        task_ptr->device_id = root["RobotId"].asInt();
        task_ptr->task_execute_id = root["TaskHistoryId"].asInt();
        task_ptr->task_id = root["TaskId"].asInt();
        task_ptr->task_type = root["TaskType"].asInt();
        task_ptr->task_name = root["task_name"].asString();
        //
        Json::Value target_array;
        target_array = root["Tasks"];
        for (int i = 0; i < target_array.size(); i++)
        {
            Json::Value tItem;
            tItem = target_array[i];
            std::string camera_pose_str = tItem["CameraPose"].asString();
            std::vector<std::string> camera_poses;
            SplitString(camera_pose_str, ";", camera_poses);
            for (size_t j = 0; j < camera_poses.size(); j++)
            {
                std::vector<std::string> points;
                SplitString(camera_poses[j], "/", points);
                std::vector<std::string> point_infos;
                SplitString(points[0], ":", point_infos);
                image_target_t target;
                target.target_id = atoi(point_infos[0].c_str());
                target.target_type = atoi(point_infos[1].c_str());
                target.pose = points[1];
                task_ptr->targets.push_back(target);
            }
        }
        image_task_ptr = nullptr;
        image_task_ptr = std::move(task_ptr);
        //
        std::unique_ptr<image_item_info_t> item_ptr(new image_item_info_t());
        image_item_info_ptr = nullptr;
        image_item_info_ptr = std::move(item_ptr);
        image_item_info_ptr->set(EImageItemStep::Init);
        //
        task.set(image_task_ptr->device_id, image_task_ptr->task_execute_id, image_task_ptr->task_id, image_task_ptr->task_type, image_task_ptr->task_name);
        status = EStatus::Load;

        return 0;
    }

    void CImageTask::init()
    {
        status = EStatus::None;
        task.reset();

        image_task_ptr = nullptr;
        image_item_info_ptr = nullptr;

        if (!b_init)
        {
            b_init = true;
            load_param();
            LaunchPublishers();
            LaunchSubscribers();
            LaunchService();
            LaunchClinet();
            LaunchActions();
            LaunchTime();
        }
    }

    void CImageTask::play(int _op)
    {
        switch (_op)
        {
        case 0:
            if (status == EStatus::Load ||
                status == EStatus::Pause)
            {
                status = EStatus::Start;
            }
            break;
        case 1:
            if (status == EStatus::Running)
            {
                status = EStatus::Pause;
            }
            break;
        case 2:
            if (status == EStatus::Running ||
                status == EStatus::Pause)
            {
                status = EStatus::Stop;
            }
            break;

        default:
            break;
        }
    }

    void CImageTask::run()
    {
        float progress = task_progress();
        task_response(task, status, progress);

        switch (status)
        {
        case EStatus::None:
            break;
        case EStatus::Load:
            /* code */
            break;
        case EStatus::Start:
            status = EStatus::Running;
            break;
        case EStatus::Running:
            task_execute();
            break;
        case EStatus::Pause:
            /* code */
            break;
        case EStatus::Stop:
            exit();
            break;
        case EStatus::Finish:
            exit();
            break;
        case EStatus::Timeout:
            exit();
            break;
        case EStatus::Exception:
            exit();
            break;
        case EStatus::Unknown:
            exit();
            break;

        default:
            break;
        }
    }

    void CImageTask::exit()
    {
        status = EStatus::None;
        task.reset();
        //
        image_task_ptr = nullptr;
        //
        image_item_info_ptr = nullptr;
    }

}
