#include "task_base.hpp"

Begin_Enum_String(blk_control::EMode)
{
    Enum_String(blk_control::EMode::None);
    Enum_String(blk_control::EMode::Fixed);
    // RegisterEnumerator(blk_control::EMode::None, "None");
    // RegisterEnumerator(blk_control::EMode::Fixed, "Fixed");
}
End_Enum_String;

namespace blk_control
{

    CTaskBase::CTaskBase(std::string _label, EMode _mode)
        : label(_label), mode(_mode), status(EStatus::None)
    {
    }

    CTaskBase::~CTaskBase()
    {
    }

    std::string CTaskBase::getLabel()
    {
        const std::string &str = EnumString<EMode>::From(mode);
        return label;
    }

    EMode CTaskBase::getMode()
    {
        return mode;
    }

    EStatus CTaskBase::getStatus()
    {
        return status;
    }

    int CTaskBase::load_task(std::string _data)
    {
        std::cout << "task base " << getLabel() << " load_task" << std::endl;
        return -1;
    }

    void CTaskBase::init()
    {
        std::cout << "task base " << getLabel() << " init" << std::endl;
    }

    void CTaskBase::play(int _op)
    {
        std::cout << "task base " << getLabel() << " play" << std::endl;
    }

    void CTaskBase::run()
    {
        std::cout << "task base " << getLabel() << " run" << std::endl;
    }

    void CTaskBase::exit()
    {
        std::cout << "task base " << getLabel() << " exit" << std::endl;
    }

}