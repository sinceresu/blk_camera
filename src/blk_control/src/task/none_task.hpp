#ifndef none_task_HPP
#define none_task_HPP

#pragma once
#include <iostream>
#include <memory>
#include <vector>
#include "../common/src/Common.hpp"
#include "task_base.hpp"

namespace blk_control
{

    class CNoneTask : public CTaskBase
    {
    public:
        CNoneTask();
        ~CNoneTask();

    public:
        int load_task(std::string _data);
        void init();
        void play(int _op);
        void run();
        void exit();
    };

}

#endif