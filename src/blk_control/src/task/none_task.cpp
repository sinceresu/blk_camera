#include "none_task.hpp"

namespace blk_control
{

    CNoneTask::CNoneTask()
        : CTaskBase("none", EMode::None)
    {
    }

    CNoneTask::~CNoneTask()
    {
    }

    int CNoneTask::load_task(std::string _data)
    {
        std::cout << "none task " << getLabel() << " load_task" << std::endl;
        return -1;
    }

    void CNoneTask::init()
    {
        std::cout << "none task " << getLabel() << " init" << std::endl;
    }

    void CNoneTask::play(int _op)
    {
        std::cout << "task base " << getLabel() << " play" << std::endl;
    }

    void CNoneTask::run()
    {
        std::cout << "none task " << getLabel() << " run" << std::endl;
    }

    void CNoneTask::exit()
    {
        std::cout << "none task " << getLabel() << " exit" << std::endl;
    }

}