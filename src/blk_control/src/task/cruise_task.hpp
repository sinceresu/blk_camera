#ifndef cruise_task_HPP
#define cruise_task_HPP

#pragma once
#include <iostream>
#include "ros/ros.h"
#include "ros/package.h"

#include "task_base.hpp"

namespace blk_control
{
    typedef struct _cruise_task_t
    {
        std::string InspectId;
        int InspectType;
        int RobotId;
        int TaskId;
        int TaskHistoryId;
        int TaskType;
    } cruise_task_t;

    class CCruiseTask : public CTaskBase
    {
    public:
        CCruiseTask();
        ~CCruiseTask();

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchClinet();
        void LaunchActions();
        void LaunchTime();

    public:
        int load_task(std::string _data);
        void init();
        void play(int _op);
        void run();
        void exit();
    };

}

#endif