#include "task_control.hpp"

namespace blk_control
{

    CTaskControl::CTaskControl(int _device_id)
        : device_id(_device_id)
    {
        init();
    }

    CTaskControl::~CTaskControl()
    {
    }

    void CTaskControl::init()
    {
        task_base_map[EMode::None] = std::make_shared<CNoneTask>();
        task_base_map[EMode::Fixed] = std::make_shared<CFixedTask>();
        task_base_map[EMode::Cruise] = std::make_shared<CCruiseTask>();
        task_base_map[EMode::Image] = std::make_shared<CImageTask>();
        task_base_map[EMode::Image2] = std::make_shared<CImage2Task>();
        set_mode(EMode::None);
    }

    void CTaskControl::add_mode(std::shared_ptr<CTaskBase> _task_base)
    {
        task_base_map[_task_base->getMode()] = _task_base;
    }

    void CTaskControl::set_mode(EMode type)
    {
        if (current_task != nullptr)
        {
            if (current_task->getMode() != type)
            {
                current_task->exit();
            }
            else
            {
                return;
            }
        }
        current_task = task_base_map[type];
        current_task->init();
    }

    EMode CTaskControl::get_mode()
    {
        if (current_task == nullptr)
            return EMode::None;

        return current_task->getMode();
    }

    std::shared_ptr<CTaskBase> CTaskControl::task()
    {
        return current_task;
    }

    int CTaskControl::parse_task(std::string _json)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_json, root))
        {
            return -1;
        }

        int device_id = root["RobotId"].asInt();
        int task_execute_id = root["TaskHistoryId"].asInt();
        int task_id = root["TaskId"].asInt();
        int task_type = root["TaskType"].asInt();
        std::string task_name = root["task_name"].asString();
        int nRet = -1;
        switch (task_type)
        {
        case 0:
            set_mode(EMode::Image);
            nRet = current_task->load_task(_json);
            break;
        case 1:
            set_mode(EMode::Fixed);
            nRet = current_task->load_task(_json);
            break;
        case 2:
            set_mode(EMode::Image2);
            nRet = current_task->load_task(_json);
            break;
        default:
            break;
        }

        return nRet;
    }

}