#include "fixed_task.hpp"

namespace blk_control
{

    CFixedTask::CFixedTask()
        : b_init(false), CTaskBase("fixed", EMode::Fixed)
    {
    }

    CFixedTask::~CFixedTask()
    {
    }

    void CFixedTask::load_param()
    {
        ros::param::get("~device_id", fixed_options.device_id);
        //
        ros::param::get("~call_ptz_pose", fixed_options.call_ptz_pose);
        ros::param::get("~ptz_pose_isreach", fixed_options.ptz_pose_isreach);
        ros::param::get("~ptz_visiable", fixed_options.ptz_visiable);
        ros::param::get("~ptz_infrared", fixed_options.ptz_infrared);
        ros::param::get("~ptz_temperature", fixed_options.ptz_temperature);
        ros::param::get("~ptz_visiable_status", fixed_options.ptz_visiable_status);
        ros::param::get("~ptz_infrared_status", fixed_options.ptz_infrared_status);
        ros::param::get("~ptz_temperature_status", fixed_options.ptz_temperature_status);
    }

    void CFixedTask::LaunchPublishers()
    {
    }

    void CFixedTask::LaunchSubscribers()
    {
        ptz_pose_isreach_subscriber = nh.subscribe(fixed_options.ptz_pose_isreach, 1, &CFixedTask::ptz_pose_isreach_Callback, this);
        ptz_visiable_Subscriber = nh.subscribe(fixed_options.ptz_visiable, 1, &CFixedTask::ptz_visiable_Callback, this);
        ptz_infrared_Subscriber = nh.subscribe(fixed_options.ptz_infrared, 1, &CFixedTask::ptz_infrared_Callback, this);
        ptz_temperature_Subscriber = nh.subscribe(fixed_options.ptz_temperature, 1, &CFixedTask::ptz_temperature_Callback, this);
    }

    void CFixedTask::LaunchService()
    {
    }

    void CFixedTask::LaunchClinet()
    {
        ptz_pose_client = nh.serviceClient<fixed_msg::cp_control>(fixed_options.call_ptz_pose);
        ptz_visiable_status_client = nh.serviceClient<data_sync_msgs::VisiableDataStatus>(fixed_options.ptz_visiable_status);
        ptz_infrared_status_client = nh.serviceClient<data_sync_msgs::InfraredDataStatus>(fixed_options.ptz_infrared_status);
        ptz_temperature_status_client = nh.serviceClient<data_sync_msgs::TemperatureDataStatus>(fixed_options.ptz_temperature_status);
    }

    void CFixedTask::LaunchActions()
    {
    }

    void CFixedTask::LaunchTime()
    {
    }

    void CFixedTask::ptz_pose_isreach_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        if (fixed_item_info_ptr && fixed_item_info_ptr->step() == EFixedItemStep::PTZ_Action_Motion)
        {
            if (msg->data == 1)
            {
                fixed_item_info_ptr->set(EFixedItemStep::PTZ_Action_Done);
            }
        }
    }

    void CFixedTask::ptz_visiable_Callback(const data_sync_msgs::VisiablePTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (fixed_item_info_ptr && fixed_item_info_ptr->step() == EFixedItemStep::Collect_Visiable_Wait)
        {
            //
            ptz_visiable_count--;
            if (ptz_visiable_count <= 0)
            {
                fixed_item_info_ptr->set(EFixedItemStep::Collect_Visiable_Done);
            }
        }
    }

    void CFixedTask::ptz_infrared_Callback(const data_sync_msgs::InfraredPTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (fixed_item_info_ptr && fixed_item_info_ptr->step() == EFixedItemStep::Collect_Infrared_Wait)
        {
            //
            ptz_infrared_count--;
            if (ptz_infrared_count <= 0)
            {
                fixed_item_info_ptr->set(EFixedItemStep::Collect_Infrared_Done);
            }
        }
    }

    void CFixedTask::ptz_temperature_Callback(const data_sync_msgs::TemperaturePTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (fixed_item_info_ptr && fixed_item_info_ptr->step() == EFixedItemStep::Collect_Temperature_Wait)
        {
            //
            ptz_temperature_count--;
            if (ptz_temperature_count <= 0)
            {
                fixed_item_info_ptr->set(EFixedItemStep::Collect_Temperature_Done);
            }
        }
    }

    bool CFixedTask::call_ptz_pose_service(int _device_id, int _id, int _action, int _type,
                                           double _value, double _x, double _y, double _z)
    {
        bool bRet = false;
        fixed_msg::cp_control msg;
        msg.request.device_id = fixed_options.device_id;
        msg.request.id = _id;
        msg.request.action = _action;
        msg.request.type = _type;
        msg.request.value = _value;
        msg.request.allvalue.push_back(_x);
        msg.request.allvalue.push_back(_y);
        msg.request.allvalue.push_back(_z);
        ROS_INFO_STREAM("call_ptz_pose_service request:\n"
                        << msg.request);
        if (ptz_pose_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_pose_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_pose_service failed");
        }

        return bRet;
    }

    bool CFixedTask::call_ptz_visiable_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::VisiableDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_visiable_status_service request:\n"
                        << msg.request);
        if (ptz_visiable_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_visiable_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_visiable_status_service failed");
        }

        return bRet;
    }

    bool CFixedTask::call_ptz_infrared_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::InfraredDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_infrared_status_service request:\n"
                        << msg.request);
        if (ptz_infrared_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_infrared_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_infrared_status_service failed");
        }

        return bRet;
    }

    bool CFixedTask::call_ptz_temperature_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::TemperatureDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_temperature_status_service request:\n"
                        << msg.request);
        if (ptz_temperature_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_temperature_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_temperature_status_service failed");
        }

        return bRet;
    }

    bool CFixedTask::timeout_tick(time_t _start_time, int _timeout_ms)
    {
        time_t now_time = GetTimeStamp();
        if ((now_time - _start_time) > _timeout_ms)
        {
            return true;
        }
        return false;
    }

    int CFixedTask::task_response(Task_t task, EStatus _status, float progress)
    {
        std::cout << "task " << getLabel() << std::endl;
        std::cout << task.device_id << " "
                  << task.task_execute_id << " "
                  << task.task_id << " "
                  << task.task_name << " "
                  << task.task_type << std::endl;
        return 0;
    }

    int CFixedTask::task_progress()
    {
        if (fixed_task_ptr == nullptr)
            return -1;
        if (fixed_item_info_ptr == nullptr)
            return -1;

        int item_count = fixed_task_ptr->targets.size();
        int item_index = fixed_item_info_ptr->index();
        if (item_count <= 0 || item_index < 0 || item_index >= item_count)
            return -1;

        int progress = item_index * item_count * 0.01;

        return progress;
    }

    int CFixedTask::task_execute()
    {
        if (fixed_task_ptr == nullptr)
            return -1;
        if (fixed_item_info_ptr == nullptr)
            return -1;

        int item_count = fixed_task_ptr->targets.size();
        int item_index = fixed_item_info_ptr->index();
        if (item_count <= 0 || item_index < 0 || item_index >= item_count)
            return -1;

        switch (fixed_item_info_ptr->step())
        {
        case EFixedItemStep::Init:
        {
            fixed_item_info_ptr->set(EFixedItemStep::PTZ_Action_Control);
        }
        break;
        case EFixedItemStep::PTZ_Action_Control:
        {
            fixed_target_t &target = fixed_task_ptr->targets[item_index];
            std::vector<std::string> pose;
            SplitString(target.pose, ",", pose);
            float pose_x = atof(pose[0].c_str());
            float pose_y = atof(pose[1].c_str());
            float pose_z = atof(pose[2].c_str());
            if (call_ptz_pose_service(fixed_options.device_id, 1, 1, 3, 0, pose_x, pose_y, pose_z))
            {
                fixed_item_info_ptr->set(EFixedItemStep::PTZ_Action_Motion);
            }
            else
            {
                fixed_item_info_ptr->set(EFixedItemStep::Exception);
            }
        }
        break;
        case EFixedItemStep::PTZ_Action_Motion:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 30 * 1000))
            {
                fixed_item_info_ptr->set(EFixedItemStep::Timeout);
            }
        }
        break;
        case EFixedItemStep::PTZ_Action_Done:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 3 * 1000))
            {
                fixed_target_t &target = fixed_task_ptr->targets[item_index];
                if (target.target_type == 0 || target.target_type == 3)
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Collect_Visiable);
                }
                if (target.target_type == 1)
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Collect_Infrared);
                }
                if (target.target_type == 2)
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Collect_Temperature);
                }
            }
        }
        break;
        case EFixedItemStep::Collect_Visiable:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 40 * 1000))
            {
                ptz_visiable_count = 1;
                if (call_ptz_visiable_status_service(fixed_task_ptr->device_id, 1, 0, ptz_visiable_count))
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Collect_Visiable_Wait);
                }
                else
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Exception);
                }
            }
        }
        break;
        case EFixedItemStep::Collect_Visiable_Wait:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_visiable_status_service(fixed_task_ptr->device_id, 1, 0, 0);
                fixed_item_info_ptr->set(EFixedItemStep::Timeout);
            }
        }
        break;
        case EFixedItemStep::Collect_Visiable_Done:
        {
            call_ptz_visiable_status_service(fixed_task_ptr->device_id, 1, 0, 0);
            fixed_target_t &target = fixed_task_ptr->targets[item_index];
            if (target.target_type == 3)
            {
                fixed_item_info_ptr->set(EFixedItemStep::Collect_Temperature);
            }
            else
            {
                fixed_item_info_ptr->set(EFixedItemStep::Finish);
            }
        }
        break;
        case EFixedItemStep::Collect_Infrared:
        {
            ptz_infrared_count = 1;
            if (call_ptz_infrared_status_service(fixed_task_ptr->device_id, 1, 0, ptz_infrared_count))
            {
                fixed_item_info_ptr->set(EFixedItemStep::Collect_Infrared_Wait);
            }
            else
            {
                fixed_item_info_ptr->set(EFixedItemStep::Exception);
            }
        }
        break;
        case EFixedItemStep::Collect_Infrared_Wait:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_infrared_status_service(fixed_task_ptr->device_id, 1, 0, 0);
                fixed_item_info_ptr->set(EFixedItemStep::Timeout);
            }
        }
        break;
        case EFixedItemStep::Collect_Infrared_Done:
        {
            call_ptz_infrared_status_service(fixed_task_ptr->device_id, 1, 0, 0);
            fixed_item_info_ptr->set(EFixedItemStep::Finish);
        }
        break;
        case EFixedItemStep::Collect_Temperature:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 1 * 1000))
            {
                ptz_temperature_count = 1;
                if (call_ptz_temperature_status_service(fixed_task_ptr->device_id, 1, 0, ptz_temperature_count))
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Collect_Temperature_Wait);
                }
                else
                {
                    fixed_item_info_ptr->set(EFixedItemStep::Exception);
                }
            }
        }
        break;
        case EFixedItemStep::Collect_Temperature_Wait:
        {
            if (timeout_tick(fixed_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_temperature_status_service(fixed_task_ptr->device_id, 1, 0, 0);
                fixed_item_info_ptr->set(EFixedItemStep::Timeout);
            }
        }
        break;
        case EFixedItemStep::Collect_Temperature_Done:
        {
            call_ptz_temperature_status_service(fixed_task_ptr->device_id, 1, 0, 0);
            fixed_item_info_ptr->set(EFixedItemStep::Finish);
        }
        break;
        case EFixedItemStep::Finish:
        case EFixedItemStep::Timeout:
        case EFixedItemStep::Exception:
        case EFixedItemStep::Unknown:
            if (item_index == (item_count - 1))
            {
                status = EStatus::Finish;
            }
            else
            {
                fixed_item_info_ptr->next();
            }
            break;

        default:
            fixed_item_info_ptr->next();
            break;
        }

        return 0;
    }

    int CFixedTask::load_task(std::string _json)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_json, root))
        {
            return -1;
        }
        std::unique_ptr<fixed_task_t> task_ptr(new fixed_task_t());
        task_ptr->device_id = root["RobotId"].asInt();
        task_ptr->task_execute_id = root["TaskHistoryId"].asInt();
        task_ptr->task_id = root["TaskId"].asInt();
        task_ptr->task_type = root["TaskType"].asInt();
        task_ptr->task_name = root["task_name"].asString();
        //
        Json::Value target_array;
        target_array = root["Tasks"];
        for (int i = 0; i < target_array.size(); i++)
        {
            Json::Value tItem;
            tItem = target_array[i];
            std::string camera_pose_str = tItem["CameraPose"].asString();
            std::vector<std::string> camera_poses;
            SplitString(camera_pose_str, ";", camera_poses);
            for (size_t j = 0; j < camera_poses.size(); j++)
            {
                std::vector<std::string> points;
                SplitString(camera_poses[j], "/", points);
                std::vector<std::string> point_infos;
                SplitString(points[0], ":", point_infos);
                fixed_target_t target;
                target.target_id = atoi(point_infos[0].c_str());
                target.target_type = atoi(point_infos[1].c_str());
                target.pose = points[1];
                task_ptr->targets.push_back(target);
            }
        }
        fixed_task_ptr = nullptr;
        fixed_task_ptr = std::move(task_ptr);
        //
        std::unique_ptr<fixed_item_info_t> item_ptr(new fixed_item_info_t());
        fixed_item_info_ptr = nullptr;
        fixed_item_info_ptr = std::move(item_ptr);
        fixed_item_info_ptr->set(EFixedItemStep::Init);
        //
        task.set(fixed_task_ptr->device_id, fixed_task_ptr->task_execute_id, fixed_task_ptr->task_id, fixed_task_ptr->task_type, fixed_task_ptr->task_name);
        status = EStatus::Load;

        return 0;
    }

    void CFixedTask::init()
    {
        status = EStatus::None;
        task.reset();

        fixed_task_ptr = nullptr;
        fixed_item_info_ptr = nullptr;

        if (!b_init)
        {
            b_init = true;
            load_param();
            LaunchPublishers();
            LaunchSubscribers();
            LaunchService();
            LaunchClinet();
            LaunchActions();
            LaunchTime();
        }
    }

    void CFixedTask::play(int _op)
    {
        switch (_op)
        {
        case 0:
            if (status == EStatus::Load ||
                status == EStatus::Pause)
            {
                status = EStatus::Start;
            }
            break;
        case 1:
            if (status == EStatus::Running)
            {
                status = EStatus::Pause;
            }
            break;
        case 2:
            if (status == EStatus::Running ||
                status == EStatus::Pause)
            {
                status = EStatus::Stop;
            }
            break;

        default:
            break;
        }
    }

    void CFixedTask::run()
    {
        float progress = task_progress();
        task_response(task, status, progress);

        switch (status)
        {
        case EStatus::None:
            break;
        case EStatus::Load:
            /* code */
            break;
        case EStatus::Start:
            status = EStatus::Running;
            break;
        case EStatus::Running:
            task_execute();
            break;
        case EStatus::Pause:
            /* code */
            break;
        case EStatus::Stop:
            exit();
            break;
        case EStatus::Finish:
            exit();
            break;
        case EStatus::Timeout:
            exit();
            break;
        case EStatus::Exception:
            exit();
            break;
        case EStatus::Unknown:
            exit();
            break;

        default:
            break;
        }
    }

    void CFixedTask::exit()
    {
        status = EStatus::None;
        task.reset();
        //
        fixed_task_ptr = nullptr;
        //
        fixed_item_info_ptr = nullptr;
    }

}
