#include "image2_task.hpp"

namespace blk_control
{

    CImage2Task::CImage2Task()
        : b_init(false), CTaskBase("image2", EMode::Image2)
    {
    }

    CImage2Task::~CImage2Task()
    {
    }

    void CImage2Task::load_param()
    {
        ros::param::get("~device_id", image2_options.device_id);
        //
        ros::param::get("~call_ptz_pose", image2_options.call_ptz_pose);
        ros::param::get("~ptz_pose_isreach", image2_options.ptz_pose_isreach);
        ros::param::get("~ptz_visiable", image2_options.ptz_visiable);
        ros::param::get("~ptz_infrared", image2_options.ptz_infrared);
        ros::param::get("~ptz_visiable_status", image2_options.ptz_visiable_status);
        ros::param::get("~ptz_infrared_status", image2_options.ptz_infrared_status);
        ros::param::get("~ptocreator_start", image2_options.ptocreator_start);
        ros::param::get("~ptocreator_stop", image2_options.ptocreator_stop);
        ros::param::get("~ptocreator_collect", image2_options.ptocreator_collect);
    }

    void CImage2Task::LaunchPublishers()
    {
    }

    void CImage2Task::LaunchSubscribers()
    {
        ptz_pose_isreach_subscriber = nh.subscribe(image2_options.ptz_pose_isreach, 1, &CImage2Task::ptz_pose_isreach_Callback, this);
        ptz_visiable_Subscriber = nh.subscribe(image2_options.ptz_visiable, 1, &CImage2Task::ptz_visiable_Callback, this);
        ptz_infrared_Subscriber = nh.subscribe(image2_options.ptz_infrared, 1, &CImage2Task::ptz_infrared_Callback, this);
    }

    void CImage2Task::LaunchService()
    {
    }

    void CImage2Task::LaunchClinet()
    {
        ptz_pose_client = nh.serviceClient<fixed_msg::cp_control>(image2_options.call_ptz_pose);
        ptz_visiable_status_client = nh.serviceClient<data_sync_msgs::InfraredDataStatus>(image2_options.ptz_visiable_status);
        ptz_infrared_status_client = nh.serviceClient<data_sync_msgs::RadarDataStatus>(image2_options.ptz_infrared_status);
        //
        ptocreator_start_client = nh.serviceClient<ptocreator::start>(image2_options.ptocreator_start);
        ptocreator_stop_client = nh.serviceClient<ptocreator::stop>(image2_options.ptocreator_stop);
        ptocreator_collect_client = nh.serviceClient<ptocreator::collect>(image2_options.ptocreator_collect);
    }

    void CImage2Task::LaunchActions()
    {
    }

    void CImage2Task::LaunchTime()
    {
    }

    void CImage2Task::ptz_pose_isreach_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        if (image2_item_info_ptr && image2_item_info_ptr->step() == EImage2ItemStep::PTZ_Action_Motion)
        {
            if (msg->data == 1)
            {
                image2_item_info_ptr->set(EImage2ItemStep::PTZ_Action_Done);
            }
        }
    }

    void CImage2Task::ptz_visiable_Callback(const data_sync_msgs::VisiablePTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (image2_task_ptr && image2_item_info_ptr && image2_item_info_ptr->step() == EImage2ItemStep::Collect_Visiable_Wait)
        {
            // std::string srcFile = std::to_string(GetTimeStamp()) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-img.jpg";

            std::string srcFile = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
                                  std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-visiable_img.jpg";
            // image2_target_t &target = image2_task_ptr->targets[image2_item_info_ptr->index()];
            // std::vector<std::string> pose;
            // SplitString(target.pose, ",", pose);
            // float pose_x = atof(pose[0].c_str());
            // float pose_y = atof(pose[1].c_str());
            // float pose_z = atof(pose[2].c_str());
            // std::string srcFile = std::to_string(pose_x) + "-" + std::to_string(pose_y) + "-visiable_img.jpg";
            image2_item_info_ptr->datas.push_back(std::to_string(_msg->ptz_pose.pose.pose.position.x / 100));
            image2_item_info_ptr->datas.push_back(std::to_string(_msg->ptz_pose.pose.pose.position.z / 100));
            image2_item_info_ptr->datas.push_back(srcFile);
            std::cout << "[ptz_visiable_Callback] srcFile:" << srcFile << std::endl;
            cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(_msg->visiable, sensor_msgs::image_encodings::TYPE_8UC3);
            cv::Mat img = cv_ptr->image;
            cv::imwrite("/home/lh/workspace/test_data/blk_img/" + srcFile, img);

            image2_item_info_ptr->set(EImage2ItemStep::Collect_Visiable_Done);
        }
    }

    void CImage2Task::ptz_infrared_Callback(const data_sync_msgs::InfraredPTZ::ConstPtr &_msg)
    {
        // ROS_INFO_STREAM(*_msg);

        if (image2_task_ptr && image2_item_info_ptr && image2_item_info_ptr->step() == EImage2ItemStep::Collect_Infrared_Wait)
        {
            // std::string srcFile = std::to_string(GetTimeStamp()) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
            //                       std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-img.jpg";

            std::string srcFile = std::to_string(_msg->ptz_pose.pose.pose.position.x) + "-" +
                                  std::to_string(_msg->ptz_pose.pose.pose.position.z) + "-infrared_img.jpg";
            // image2_target_t &target = image2_task_ptr->targets[image2_item_info_ptr->index()];
            // std::vector<std::string> pose;
            // SplitString(target.pose, ",", pose);
            // float pose_x = atof(pose[0].c_str());
            // float pose_y = atof(pose[1].c_str());
            // float pose_z = atof(pose[2].c_str());
            // std::string srcFile = std::to_string(pose_x) + "-" + std::to_string(pose_y) + "-infrared_img.jpg";
            image2_item_info_ptr->datas.push_back(std::to_string(_msg->ptz_pose.pose.pose.position.x / 100));
            image2_item_info_ptr->datas.push_back(std::to_string(_msg->ptz_pose.pose.pose.position.z / 100));
            image2_item_info_ptr->datas.push_back(srcFile);
            std::cout << "[ptz_infrared_Callback] srcFile:" << srcFile << std::endl;
            cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(_msg->infrared, sensor_msgs::image_encodings::TYPE_8UC3);
            cv::Mat img = cv_ptr->image;
            cv::imwrite("/home/lh/workspace/test_data/blk_img/" + srcFile, img);

            image2_item_info_ptr->set(EImage2ItemStep::Collect_Infrared_Done);
        }
    }

    bool CImage2Task::call_ptz_pose_service(int _device_id, int _id, int _action, int _type,
                                            double _value, double _x, double _y, double _z)
    {
        bool bRet = false;
        fixed_msg::cp_control msg;
        msg.request.device_id = image2_options.device_id;
        msg.request.id = _id;
        msg.request.action = _action;
        msg.request.type = _type;
        msg.request.value = _value;
        msg.request.allvalue.push_back(_x);
        msg.request.allvalue.push_back(_y);
        msg.request.allvalue.push_back(_z);
        if (ptz_pose_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_pose_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_pose_service failed");
        }

        return bRet;
    }

    bool CImage2Task::call_ptz_visiable_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::VisiableDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_visiable_status_service request:\n"
                        << msg.request);
        if (ptz_visiable_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_visiable_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_visiable_status_service failed");
        }

        return bRet;
    }

    bool CImage2Task::call_ptz_infrared_status_service(int _device_id, int _id, int _status, int _num)
    {
        bool bRet = false;
        data_sync_msgs::InfraredDataStatus msg;
        msg.request.device_id = _device_id;
        msg.request.camera_id = _id;
        msg.request.status = _status;
        msg.request.values.push_back(std::to_string(_num));
        ROS_INFO_STREAM("call_ptz_infrared_status_service request:\n"
                        << msg.request);
        if (ptz_infrared_status_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptz_infrared_status_service response:\n"
                            << msg.response);
            if (msg.response.result == 0)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptz_infrared_status_service failed");
        }

        return bRet;
    }

    bool CImage2Task::call_ptocreator_start_service(int _device_id, int _task_id, int _map_id, int _sum)
    {
        bool bRet = false;
        ptocreator::start msg;
        msg.request.device_id = _device_id;
        msg.request.task_id = _task_id;
        msg.request.map_id = _map_id;
        msg.request.sum = _sum;
        ROS_INFO_STREAM("call_ptocreator_start_service request:\n"
                        << msg.request);
        if (ptocreator_start_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptocreator_start_service response:\n"
                            << msg.response);
            if (msg.response.success)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptocreator_start_service failed");
        }

        return bRet;
    }

    bool CImage2Task::call_ptocreator_stop_service(int _device_id, int _task_id, int _map_id)
    {
        bool bRet = false;
        ptocreator::stop msg;
        msg.request.device_id = _device_id;
        msg.request.task_id = _task_id;
        msg.request.map_id = _map_id;
        ROS_INFO_STREAM("call_ptocreator_stop_service request:\n"
                        << msg.request);
        if (ptocreator_stop_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptocreator_stop_service response:\n"
                            << msg.response);
            if (msg.response.success)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptocreator_stop_service failed");
        }

        return bRet;
    }

    bool CImage2Task::call_ptocreator_collect_service(float _roll_angle, float _pitch_angle, float _yaw_angle, std::string _imgname)
    {
        bool bRet = false;
        ptocreator::collect msg;
        msg.request.roll_angle = _roll_angle;
        msg.request.pitch_angle = _pitch_angle;
        msg.request.yaw_angle = _yaw_angle;
        msg.request.imgname = _imgname;
        ROS_INFO_STREAM("call_ptocreator_collect_service request:\n"
                        << msg.request);
        if (ptocreator_collect_client.call(msg))
        {
            ROS_INFO_STREAM("call_ptocreator_collect_service response:\n"
                            << msg.response);
            if (msg.response.success)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_ptocreator_collect_service failed");
        }

        return bRet;
    }

    bool CImage2Task::timeout_tick(time_t _start_time, int _timeout_ms)
    {
        time_t now_time = GetTimeStamp();
        if ((now_time - _start_time) > _timeout_ms)
        {
            return true;
        }
        return false;
    }

    int CImage2Task::task_response(Task_t task, EStatus _status, float progress)
    {
        std::cout << "task " << getLabel() << std::endl;
        std::cout << task.device_id << " "
                  << task.task_execute_id << " "
                  << task.task_id << " "
                  << task.task_name << " "
                  << task.task_type << std::endl;
        return 0;
    }

    int CImage2Task::task_progress()
    {
        if (image2_task_ptr == nullptr)
            return -1;
        if (image2_item_info_ptr == nullptr)
            return -1;

        int item_count = image2_task_ptr->targets.size();
        int item_index = image2_item_info_ptr->index();
        if (item_count <= 0 || item_index < 0 || item_index >= item_count)
            return -1;

        int progress = item_index * item_count * 0.01;

        return progress;
    }

    int CImage2Task::task_execute()
    {
        if (image2_task_ptr == nullptr)
            return -1;
        if (image2_item_info_ptr == nullptr)
            return -1;

        int item_count = image2_task_ptr->targets.size();
        int item_index = image2_item_info_ptr->index();
        if (item_count <= 0 || item_index < 0 || item_index >= item_count)
            return -1;

        switch (image2_item_info_ptr->step())
        {
        case EImage2ItemStep::Init:
        {
            image2_item_info_ptr->set(EImage2ItemStep::PTZ_Action_Control);
        }
        break;
        case EImage2ItemStep::PTZ_Action_Control:
        {
            image2_target_t &target = image2_task_ptr->targets[item_index];
            std::vector<std::string> pose;
            SplitString(target.pose, ",", pose);
            float pose_x = atof(pose[0].c_str());
            float pose_y = atof(pose[1].c_str());
            float pose_z = atof(pose[2].c_str());
            if (call_ptz_pose_service(image2_options.device_id, 1, 1, 3, 0, pose_x, pose_y, pose_z))
            {
                image2_item_info_ptr->set(EImage2ItemStep::PTZ_Action_Motion);
            }
            else
            {
                image2_item_info_ptr->set(EImage2ItemStep::Exception);
            }
        }
        break;
        case EImage2ItemStep::PTZ_Action_Motion:
        {
            if (timeout_tick(image2_item_info_ptr->time(), 30 * 1000))
            {
                image2_item_info_ptr->set(EImage2ItemStep::Timeout);
            }
        }
        break;
        case EImage2ItemStep::PTZ_Action_Done:
        {
            if (timeout_tick(image2_item_info_ptr->time(), 3 * 1000))
            {
                image2_target_t &target = image2_task_ptr->targets[item_index];
                if (target.target_type == 0)
                {
                    image2_item_info_ptr->set(EImage2ItemStep::Collect_Visiable);
                }
                if (target.target_type == 1)
                {
                    image2_item_info_ptr->set(EImage2ItemStep::Collect_Infrared);
                }
            }
        }
        break;
        case EImage2ItemStep::Collect_Visiable:
        {
            if (call_ptz_visiable_status_service(image2_task_ptr->device_id, 1, 0, 1))
            {
                image2_item_info_ptr->set(EImage2ItemStep::Collect_Visiable_Wait);
            }
            else
            {
                image2_item_info_ptr->set(EImage2ItemStep::Exception);
            }
        }
        break;
        case EImage2ItemStep::Collect_Visiable_Wait:
        {
            if (timeout_tick(image2_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_visiable_status_service(image2_task_ptr->device_id, 1, 0, 0);
                image2_item_info_ptr->set(EImage2ItemStep::Timeout);
            }
        }
        break;
        case EImage2ItemStep::Collect_Visiable_Done:
        {
            call_ptz_visiable_status_service(image2_task_ptr->device_id, 1, 0, 0);
            image2_item_info_ptr->set(EImage2ItemStep::Collect_Record);
        }
        break;
        case EImage2ItemStep::Collect_Infrared:
        {
            if (call_ptz_infrared_status_service(image2_task_ptr->device_id, 1, 0, 1))
            {
                image2_item_info_ptr->set(EImage2ItemStep::Collect_Infrared_Wait);
            }
            else
            {
                image2_item_info_ptr->set(EImage2ItemStep::Exception);
            }
        }
        break;
        case EImage2ItemStep::Collect_Infrared_Wait:
        {
            if (timeout_tick(image2_item_info_ptr->time(), 30 * 1000))
            {
                call_ptz_infrared_status_service(image2_task_ptr->device_id, 1, 0, 0);
                image2_item_info_ptr->set(EImage2ItemStep::Timeout);
            }
        }
        break;
        case EImage2ItemStep::Collect_Infrared_Done:
        {
            call_ptz_infrared_status_service(image2_task_ptr->device_id, 1, 0, 0);
            image2_item_info_ptr->set(EImage2ItemStep::Collect_Record);
        }
        break;
        case EImage2ItemStep::Collect_Record:
        {
            if (image2_item_info_ptr->datas.size() >= 3)
            {
                if (call_ptocreator_collect_service(0.0, atof(image2_item_info_ptr->datas[1].c_str()), atof(image2_item_info_ptr->datas[0].c_str()),
                                                    image2_item_info_ptr->datas[2]))
                {
                    image2_item_info_ptr->set(EImage2ItemStep::Finish);
                }
                else
                {
                    image2_item_info_ptr->set(EImage2ItemStep::Exception);
                }
                image2_item_info_ptr->datas.clear();
            }
            else
            {
                image2_item_info_ptr->set(EImage2ItemStep::Exception);
            }
        }
        break;
        case EImage2ItemStep::Finish:
        case EImage2ItemStep::Timeout:
        case EImage2ItemStep::Exception:
        case EImage2ItemStep::Unknown:
            if (item_index == (item_count - 1))
            {
                status = EStatus::Finish;
                call_ptocreator_stop_service(image2_task_ptr->device_id, image2_task_ptr->task_execute_id, 0);
            }
            else
            {
                image2_item_info_ptr->next();
            }
            break;

        default:
            image2_item_info_ptr->next();
            break;
        }

        return 0;
    }

    int CImage2Task::load_task(std::string _json)
    {
        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_json, root))
        {
            return -1;
        }
        std::unique_ptr<image2_task_t> task_ptr(new image2_task_t());
        task_ptr->device_id = root["RobotId"].asInt();
        task_ptr->task_execute_id = root["TaskHistoryId"].asInt();
        task_ptr->task_id = root["TaskId"].asInt();
        task_ptr->task_type = root["TaskType"].asInt();
        task_ptr->task_name = root["task_name"].asString();
        //
        Json::Value target_array;
        target_array = root["Tasks"];
        for (int i = 0; i < target_array.size(); i++)
        {
            Json::Value tItem;
            tItem = target_array[i];
            std::string camera_pose_str = tItem["CameraPose"].asString();
            std::vector<std::string> camera_poses;
            SplitString(camera_pose_str, ";", camera_poses);
            for (size_t j = 0; j < camera_poses.size(); j++)
            {
                std::vector<std::string> points;
                SplitString(camera_poses[j], "/", points);
                std::vector<std::string> point_infos;
                SplitString(points[0], ":", point_infos);
                image2_target_t target;
                target.target_id = atoi(point_infos[0].c_str());
                target.target_type = atoi(point_infos[1].c_str());
                target.pose = points[1];
                task_ptr->targets.push_back(target);
            }
        }
        image2_task_ptr = nullptr;
        image2_task_ptr = std::move(task_ptr);
        //
        std::unique_ptr<image2_item_info_t> item_ptr(new image2_item_info_t());
        image2_item_info_ptr = nullptr;
        image2_item_info_ptr = std::move(item_ptr);
        image2_item_info_ptr->set(EImage2ItemStep::Init);
        //
        task.set(image2_task_ptr->device_id, image2_task_ptr->task_execute_id, image2_task_ptr->task_id, image2_task_ptr->task_type, image2_task_ptr->task_name);
        status = EStatus::Load;
        //
        if (call_ptocreator_start_service(image2_task_ptr->device_id, image2_task_ptr->task_execute_id, 0, image2_task_ptr->targets.size()))
        {
            return -1;
        }

        return 0;
    }

    void CImage2Task::init()
    {
        status = EStatus::None;
        task.reset();

        image2_task_ptr = nullptr;
        image2_item_info_ptr = nullptr;

        if (!b_init)
        {
            b_init = true;
            load_param();
            LaunchPublishers();
            LaunchSubscribers();
            LaunchService();
            LaunchClinet();
            LaunchActions();
            LaunchTime();
        }
    }

    void CImage2Task::play(int _op)
    {
        switch (_op)
        {
        case 0:
            if (status == EStatus::Load ||
                status == EStatus::Pause)
            {
                status = EStatus::Start;
            }
            break;
        case 1:
            if (status == EStatus::Running)
            {
                status = EStatus::Pause;
            }
            break;
        case 2:
            if (status == EStatus::Running ||
                status == EStatus::Pause)
            {
                status = EStatus::Stop;
            }
            break;

        default:
            break;
        }
    }

    void CImage2Task::run()
    {
        float progress = task_progress();
        task_response(task, status, progress);

        switch (status)
        {
        case EStatus::None:
            break;
        case EStatus::Load:
            /* code */
            break;
        case EStatus::Start:
            status = EStatus::Running;
            break;
        case EStatus::Running:
            task_execute();
            break;
        case EStatus::Pause:
            /* code */
            break;
        case EStatus::Stop:
            exit();
            break;
        case EStatus::Finish:
            exit();
            break;
        case EStatus::Timeout:
            exit();
            break;
        case EStatus::Exception:
            exit();
            break;
        case EStatus::Unknown:
            exit();
            break;

        default:
            break;
        }
    }

    void CImage2Task::exit()
    {
        status = EStatus::None;
        task.reset();
        //
        image2_task_ptr = nullptr;
        //
        image2_item_info_ptr = nullptr;
    }

}
