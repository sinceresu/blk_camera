#ifndef task_control_HPP
#define task_control_HPP

#pragma once
#include <iostream>
#include <memory>
#include <map>

#include "task_base.hpp"
#include "none_task.hpp"
#include "fixed_task.hpp"
#include "cruise_task.hpp"
#include "image_task.hpp"
#include "image2_task.hpp"

namespace blk_control
{

    class CTaskControl
    {
    public:
        CTaskControl(int _device_id);
        ~CTaskControl();

    private:
        int device_id;
        std::map<EMode, std::shared_ptr<CTaskBase>> task_base_map;
        std::shared_ptr<CTaskBase> current_task;
        //
        void init();
        void add_mode(std::shared_ptr<CTaskBase> _task_base);
        void set_mode(EMode _mode);

    public:
        EMode get_mode();
        std::shared_ptr<CTaskBase> task();
        int parse_task(std::string _json);
    };

}

#endif