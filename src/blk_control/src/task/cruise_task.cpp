#include "cruise_task.hpp"

namespace blk_control
{

    CCruiseTask::CCruiseTask()
        : CTaskBase("cruise", EMode::Cruise)
    {
    }

    CCruiseTask::~CCruiseTask()
    {
    }

    void CCruiseTask::LaunchPublishers()
    {
    }

    void CCruiseTask::LaunchSubscribers()
    {
    }

    void CCruiseTask::LaunchService()
    {
    }

    void CCruiseTask::LaunchClinet()
    {
    }

    void CCruiseTask::LaunchActions()
    {
    }

    void CCruiseTask::LaunchTime()
    {
    }

    int CCruiseTask::load_task(std::string _data)
    {
        std::cout << "cruise task " << getLabel() << " load_task" << std::endl;
        return -1;
    }

    void CCruiseTask::init()
    {
        std::cout << "cruise task " << getLabel() << " init" << std::endl;
    }

    void CCruiseTask::play(int _op)
    {
        std::cout << "cruise base " << getLabel() << " play" << std::endl;
    }

    void CCruiseTask::run()
    {
        std::cout << "cruise task " << getLabel() << " run" << std::endl;
    }

    void CCruiseTask::exit()
    {
        std::cout << "cruise task " << getLabel() << " exit" << std::endl;
    }

}