#ifndef image2_task_HPP
#define image2_task_HPP

#pragma once
#include <iostream>
#include <memory>
#include <vector>
#include "ros/ros.h"
#include "ros/package.h"
#include "cv_bridge/cv_bridge.h"
#include "std_msgs/Int32.h"
#include "fixed_msg/cp_control.h"
#include "data_sync_msgs/VisiablePTZ.h"
#include "data_sync_msgs/InfraredPTZ.h"
#include "data_sync_msgs/RadarPTZ.h"
#include "data_sync_msgs/VisiableDataStatus.h"
#include "data_sync_msgs/InfraredDataStatus.h"
#include "data_sync_msgs/RadarDataStatus.h"
#include "ptocreator/start.h"
#include "ptocreator/stop.h"
#include "ptocreator/collect.h"
#include "../common/src/Common.hpp"
#include "xpack/json.h"
#include "task_base.hpp"
#include <opencv2/opencv.hpp>

namespace blk_control
{
    enum struct EImage2ErrorCode
    {
        None,
        Task_Null,
        PTZ_Error,
        Unknown
    };

    enum struct EImage2ItemStep
    {
        None,
        Init,
        PTZ_Action_Control,
        PTZ_Action_Motion,
        PTZ_Action_Done,
        Collect_Visiable,
        Collect_Visiable_Wait,
        Collect_Visiable_Done,
        Collect_Infrared,
        Collect_Infrared_Wait,
        Collect_Infrared_Done,
        Collect_Record,
        Finish,
        Timeout,
        Exception,
        Unknown
    };

    typedef struct _image2_target_t
    {
        int target_id;
        int target_type;
        std::string pose;
    } image2_target_t;

    typedef struct _image2_task_t
    {
        int device_id;
        int task_execute_id;
        int task_id;
        int task_type;
        std::string task_name;
        std::vector<image2_target_t> targets;
    } image2_task_t;

    typedef struct _image2_item_info_t
    {
    private:
        unsigned int item_index;
        EImage2ItemStep item_step;
        time_t item_time;

    public:
        _image2_item_info_t() : item_index(0), item_step(EImage2ItemStep::None), item_time(0) {}
        std::vector<std::string> datas;
        //
        void set(EImage2ItemStep _step)
        {
            // item_index;
            item_step = _step;
            item_time = GetTimeStamp();
        }
        unsigned int index()
        {
            return item_index;
        }
        EImage2ItemStep step()
        {
            return item_step;
        }
        time_t time()
        {
            return item_time;
        }
        void next()
        {
            item_index++;
            item_step = EImage2ItemStep::Init;
            item_time = GetTimeStamp();
        }
    } image2_item_info_t;

    class CImage2Task : public CTaskBase
    {
        typedef struct _Image2Options
        {
            int device_id;

            std::string call_ptz_pose;
            std::string ptz_pose_isreach;
            std::string ptz_visiable;
            std::string ptz_infrared;
            std::string ptz_visiable_status;
            std::string ptz_infrared_status;
            std::string ptocreator_start;
            std::string ptocreator_stop;
            std::string ptocreator_collect;
        } Image2Options;

    public:
        CImage2Task();
        ~CImage2Task();

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        Image2Options image2_options;

        bool b_init;

        void load_param();

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchClinet();
        void LaunchActions();
        void LaunchTime();

        ros::Subscriber ptz_pose_isreach_subscriber;
        void ptz_pose_isreach_Callback(const std_msgs::Int32::ConstPtr &msg);
        ros::Subscriber ptz_visiable_Subscriber;
        void ptz_visiable_Callback(const data_sync_msgs::VisiablePTZ::ConstPtr &_msg);
        ros::Subscriber ptz_infrared_Subscriber;
        void ptz_infrared_Callback(const data_sync_msgs::InfraredPTZ::ConstPtr &_msg);
        ros::ServiceClient ptz_pose_client;
        bool call_ptz_pose_service(int _device_id, int _id, int _action, int _type,
                                   double _value, double _x, double _y, double _z);
        ros::ServiceClient ptz_visiable_status_client;
        bool call_ptz_visiable_status_service(int _device_id, int _id, int _status, int _num);
        ros::ServiceClient ptz_infrared_status_client;
        bool call_ptz_infrared_status_service(int _device_id, int _id, int _status, int _num);
        //
        ros::ServiceClient ptocreator_start_client;
        bool call_ptocreator_start_service(int _device_id, int _task_id, int _map_id, int _sum);
        ros::ServiceClient ptocreator_stop_client;
        bool call_ptocreator_stop_service(int _device_id, int _task_id, int _map_id);
        ros::ServiceClient ptocreator_collect_client;
        bool call_ptocreator_collect_service(float _roll_angle, float _pitch_angle, float _yaw_angle, std::string _imgname);

    private:
        std::unique_ptr<image2_task_t> image2_task_ptr;
        std::unique_ptr<image2_item_info_t> image2_item_info_ptr;

    private:
        bool timeout_tick(time_t _start_time, int _timeout_ms);
        int task_response(Task_t task, EStatus _status, float progress);
        int task_progress();
        int task_execute();

    public:
        int load_task(std::string _data);
        void init();
        void play(int _op);
        void run();
        void exit();
    };

}

#endif