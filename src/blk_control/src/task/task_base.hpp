#ifndef task_base_HPP
#define task_base_HPP

#pragma once
#include <iostream>
#include <functional>
#include <jsoncpp/json/json.h>
#include "../common/src/Common.hpp"

namespace blk_control
{

    enum struct EMode
    {
        None,
        Fixed,
        Cruise,
        Image,
        Image2
    };

    enum struct EStatus
    {
        None,
        Load,
        Start,
        Running,
        Pause,
        Stop,
        Finish,
        Timeout,
        Exception,
        Unknown
    };

    typedef struct _Task_t
    {
        int device_id;
        int task_execute_id;
        int task_id;
        int task_type;
        std::string task_name;
        _Task_t() : device_id(0), task_execute_id(0),
                    task_id(0), task_type(0), task_name("") {}
        void set(int _device_id, int _task_execute_id,
                 int _task_id, int _task_type, std::string _task_name)
        {
            device_id = _device_id;
            task_execute_id = _task_execute_id;
            task_id = _task_id;
            task_type = _task_type;
            task_name = _task_name;
        }
        void reset()
        {
            device_id = 0;
            task_execute_id = 0;
            task_id = 0;
            task_type = 0;
            task_name = "";
        }
    } Task_t;

    class CTaskBase
    {
    public:
        CTaskBase(std::string _label = "", EMode _mode = EMode::None);
        ~CTaskBase();

    private:
        std::string label;
        EMode mode;

    protected:
        EStatus status;
        Task_t task;

    public:
        std::string getLabel();
        EMode getMode();
        EStatus getStatus();

    public:
        virtual int load_task(std::string _data);
        virtual void init();
        virtual void play(int _op);
        virtual void run();
        virtual void exit();
    };

}

#endif