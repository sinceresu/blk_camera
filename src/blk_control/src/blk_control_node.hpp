#ifndef blk_control_node_HPP
#define blk_control_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include <thread>
#include "ros/ros.h"
#include "ros/package.h"

#include "actionlib/client/simple_action_client.h"
#include "std_msgs/Int32.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "nav_msgs/Odometry.h"
#include "blk_control_msgs/TaskData.h"
#include "blk_control_msgs/TaskControl.h"
#include "blk_control_msgs/WorkMode.h"
#include "blk_control_msgs/SetWorkMode.h"
#include "blk_control_msgs/GetWorkMode.h"
#include "blk_control_msgs/GetPTZPose.h"
#include "blk_control_msgs/GetRadarData.h"
#include "fixed_msg/cp_control.h"

#include "blk_control.hpp"

namespace blk_control
{

    class CBLKControlNode
    {
    public:
        typedef struct _NodeOptions
        {
            int device_id;
            int camera_id;
            // msgs
            std::string task_data;
            std::string task_control;
            std::string work_mode;
            std::string set_work_mode;
            std::string ptz_pose;
            std::string ptz_pose_isreach;
            std::string radar_data;
            std::string get_work_mode;
            std::string call_ptz_pose;
            std::string get_ptz_pose;
            std::string get_radar_data;
        } NodeOptions;

    public:
        CBLKControlNode(NodeOptions _node_options);
        ~CBLKControlNode();

    private:
        NodeOptions node_options;

        std::string work_mode;
        std::string work_state;
        std::string visiable;
        std::string infrared;
        std::string temperature;
        std::string radar;

        CBLKControl blk_control;

    private:
        std::queue<nav_msgs::Odometry> ptz_pose_list;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchClinet();
        void LaunchActions();
        void LaunchTime();

        ros::Publisher work_mode_publisher;
        void publisher_work_mode(int _device_id, int _camera_id, std::string _mode);
        ros::Publisher work_state_publisher;
        void publisher_work_state(int _device_id, int _camera_id, std::string _state);

        ros::Subscriber ptz_pose_subscriber;
        void ptz_pose_Callback(const nav_msgs::Odometry::ConstPtr &msg);
        ros::Subscriber ptz_pose_isreach_subscriber;
        void ptz_pose_isreach_Callback(const std_msgs::Int32::ConstPtr &msg);

        ros::ServiceServer task_data_service;
        bool task_data_service_Callback(blk_control_msgs::TaskData::Request &req,
                                        blk_control_msgs::TaskData::Response &res);
        ros::ServiceServer task_control_service;
        bool task_control_service_Callback(blk_control_msgs::TaskControl::Request &req,
                                           blk_control_msgs::TaskControl::Response &res);
        ros::ServiceServer set_work_mode_service;
        bool set_work_mode_service_Callback(blk_control_msgs::SetWorkMode::Request &req,
                                            blk_control_msgs::SetWorkMode::Response &res);
        ros::ServiceServer get_work_mode_service;
        bool get_work_mode_service_Callback(blk_control_msgs::GetWorkMode::Request &req,
                                            blk_control_msgs::GetWorkMode::Response &res);
        ros::ServiceServer get_ptz_pose_service;
        bool get_ptz_pose_service_Callback(blk_control_msgs::GetPTZPose::Request &req,
                                           blk_control_msgs::GetPTZPose::Response &res);
        ros::ServiceServer get_radar_data_service;
        bool get_radar_data_service_Callback(blk_control_msgs::GetRadarData::Request &req,
                                             blk_control_msgs::GetRadarData::Response &res);

        ros::ServiceClient ptz_pose_client;
        bool call_ptz_pose_service(int _device_id, int _camera_id, int _id, int _action, int _type,
                                   double _value, double _x, double _y, double _z);

        void tick(const ros::TimerEvent &event);
        void update_data(void *p);
    };

}

#endif