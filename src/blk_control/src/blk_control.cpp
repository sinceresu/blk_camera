#include "blk_control.hpp"

namespace blk_control
{

    CBLKControl::CBLKControl()
    {
        init_task_controller();

        std::thread task_Thread = std::thread(std::bind(&CBLKControl::task_loop, this, nullptr));
        task_Thread.detach();
    }

    CBLKControl::~CBLKControl()
    {
    }

    void CBLKControl::init_task_controller()
    {
        // task_control_ptr = std::make_unique<CTaskControl>();
        std::unique_ptr<CTaskControl> temp(new CTaskControl(0));
        task_control_ptr = std::move(temp);
    }

    int CBLKControl::add_task(std::string _data)
    {
        // test
        // /home/lihao/Workspace/blk_camera/src/blk_control/config/RobotId_TaskHistoryId_TaskId.json
        std::ifstream fs(_data, std::ifstream::binary);
        if (!fs)
        {
            return -1;
        }
        std::string _tmp((std::istreambuf_iterator<char>(fs)), std::istreambuf_iterator<char>());
        int nRet = task_control_ptr->parse_task(_tmp);
        // --
        // int nRet = task_control_ptr->parse_task(_data);

        return nRet;
    }

    int CBLKControl::con_task(int _status)
    {
        task_control_ptr->task()->play(_status);

        return 0;
    }

    void CBLKControl::task_loop(void *p)
    {
        while (1)
        {
            task_control_ptr->task()->run();

            usleep(1000 * 1000);
        }
    }

}