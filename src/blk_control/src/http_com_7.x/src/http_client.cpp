#include "http_client.hpp"
#include "mongoose.h"

typedef struct _request_info_t
{
  const char *s_url;
  const char *s_type;
  const char *s_header;
  const char *s_data;
  int n_lenght;
  void *user_data;
  bool b_done;
  DataCallback resultCallback;
} request_info_t;

CHttpClient::CHttpClient()
{
}

CHttpClient::~CHttpClient()
{
}

void CHttpClient::ev_handler(struct mg_connection *c, int ev, void *ev_data, void *fn_data)
{
  request_info_t *request_info = (request_info_t *)fn_data;

  if (ev == MG_EV_CONNECT)
  {
    // Connected to server. Extract host name from URL
    // struct mg_str host = mg_url_host(s_url);
    struct mg_str host = mg_url_host(request_info->s_url);

    // If s_url is https://, tell client connection to use TLS
    // if (mg_url_is_ssl(s_url))
    if (mg_url_is_ssl(request_info->s_url))
    {
      // struct mg_tls_opts opts = {.ca = "ca.pem", .srvname = host};
      struct mg_tls_opts opts;
      opts.ca = "ca.pem";
      opts.srvname = host;
      mg_tls_init(c, &opts);
    }

    // Send request
    // mg_printf(c,
    //           "GET %s HTTP/1.0\r\n"
    //           "Host: %.*s\r\n"
    //           "\r\n",
    //           mg_url_uri(s_url), (int)host.len, host.ptr);
    mg_printf(c,
              "%s %s HTTP/1.1\r\n"
              "Host: %.*s\r\n"
              "%s"
              "Content-Length: %d\r\n"
              "\r\n"
              "%s",
              request_info->s_type, mg_url_uri(request_info->s_url), (int)host.len, host.ptr, request_info->s_header, request_info->n_lenght, request_info->s_data);
  }
  else if (ev == MG_EV_HTTP_MSG)
  {
    // Response is received. Print it
    struct mg_http_message *hm = (struct mg_http_message *)ev_data;
    // printf("%.*s \r\n", (int)hm->message.len, hm->message.ptr);
    c->is_closing = 1; // Tell mongoose to close this connection
    response(0, hm->body.ptr, hm->body.len, fn_data);
    // *(bool *)fn_data = true; // Tell event loop to stop
    request_info->b_done = true;
  }
  else if (ev == MG_EV_ERROR)
  {
    response(-1, "", 0, fn_data);
    // *(bool *)fn_data = true; // Error, tell event loop to stop
    request_info->b_done = true;
  }
}

void CHttpClient::request(const char *type, const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data)
{
  request_info_t request_info;
  request_info.s_url = url;
  request_info.s_type = type;
  request_info.s_header = headers;
  request_info.s_data = data;
  request_info.n_lenght = lenght;
  request_info.b_done = false;
  request_info.user_data = user_data;
  request_info.resultCallback = callback;

  struct mg_mgr mgr; // Event manager
  // bool done = false;                       // Event handler flips it to true
  request_info.b_done = false;
  mg_log_set("3");   // Set to 0 to disable debug
  mg_mgr_init(&mgr); // Initialise event manager
  // mg_http_connect(&mgr, s_url, fn, &done); // Create client connection
  mg_http_connect(&mgr, url, CHttpClient::ev_handler, &request_info); // Create client connection
  // while (!done)
  while (!request_info.b_done)
    mg_mgr_poll(&mgr, 1000); // Infinite event loop
  mg_mgr_free(&mgr);         // Free resources
}

void CHttpClient::response(int code, const char *response, int lenght, void *fn_data)
{
  request_info_t *request_info = (request_info_t *)fn_data;
  if (request_info->resultCallback != NULL)
  {
    request_info->resultCallback(code, response, lenght, request_info->user_data);
  }
  request_info->b_done = true;
}

void CHttpClient::get(const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data)
{
  // printf("http client get request. url:%s, header:%s, data:%s lenght:%d ... \r\n", url, headers, data, lenght);

  request("GET", url, headers, data, lenght, callback, user_data);
}

void CHttpClient::post(const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data)
{
  // printf("http client post request. url:%s, header:%s, data:%s lenght:%d ... \r\n", url, headers, data, lenght);

  request("POST", url, headers, data, lenght, callback, user_data);
}

void CHttpClient::put(const char *url, const char *headers, const char *data, const int lenght, DataCallback callback, void *user_data)
{
  // printf("http client put request. url:%s, header:%s, data:%s lenght:%d ... \r\n", url, headers, data, lenght);

  request("PUT", url, headers, data, lenght, callback, user_data);
}

// CHttpRequest
CHttpRequest::CHttpRequest()
{
}

CHttpRequest::~CHttpRequest()
{
}

int CHttpRequest::get(std::string address, int port, std::string url, std::string headers, std::string data, std::string &response)
{
  int nRet = 0;
  try
  {
    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in ServerAddr;
    memset(&ServerAddr, 0, sizeof(ServerAddr));
    ServerAddr.sin_addr.s_addr = inet_addr(address.c_str());
    ServerAddr.sin_port = htons(port);
    ServerAddr.sin_family = AF_INET;
    int ret = connect(clientSocket, (sockaddr *)&ServerAddr, sizeof(ServerAddr));
    if (ret == 0)
    {
      int len = data.length();
      std::string request;
      request = boost::str(boost::format("GET %s HTTP/1.1\r\nHost: %s:%d\r\n%sCookie: 16888\r\nAccept: application/json\r\nConnection: keep-alive\r\nContent-Type: application/json\r\nCharset: utf-8\r\nContent-Length: %d\r\n\r\n%s") % url % address % port % headers % len % data);
      printf("http get request: %s \r\n", request.c_str());
      ret = send(clientSocket, request.c_str(), request.length(), 0);
      if (ret > 0)
      {
        int bufLen = 1024 * 2;
        char bufRecv[bufLen] = {0};
        ret = recv(clientSocket, bufRecv, bufLen, 0);
        if (ret > 0)
        {
          response = bufRecv;
          printf("http get response: %s.\r\n", bufRecv);
        }
        else
        {
          nRet = -1;
          printf("\033[31m http get connect recv, code: %d. \r\n \033[0m", ret);
        }
      }
      else
      {
        nRet = -1;
        printf("\033[0m http get send error, code: %d. \r\n \033[0m", ret);
      }
    }
    else
    {
      nRet = -1;
      printf("\033[0m http get connect error, code: %d. \r\n \033[0m", ret);
    }
    close(clientSocket);
  }
  catch (...)
  {
    nRet = -1;
    printf("\033[0m http get error. \r\n \033[0m");
  }

  return nRet;
}

int CHttpRequest::post(std::string address, int port, std::string url, std::string headers, std::string data, std::string &response)
{
  int nRet = 0;
  try
  {
    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in ServerAddr;
    memset(&ServerAddr, 0, sizeof(ServerAddr));
    ServerAddr.sin_addr.s_addr = inet_addr(address.c_str());
    ServerAddr.sin_port = htons(port);
    ServerAddr.sin_family = AF_INET;
    int ret = connect(clientSocket, (sockaddr *)&ServerAddr, sizeof(ServerAddr));
    if (ret == 0)
    {
      int len = data.length();
      std::string request;
      request = boost::str(boost::format("POST %s HTTP/1.1\r\nHost: %s:%d\r\n%sCookie: 16888\r\nAccept: application/json\r\nConnection: keep-alive\r\nContent-Type: application/json\r\nCharset: utf-8\r\nContent-Length: %d\r\n\r\n%s") % url % address % port % headers % len % data);
      printf("http post request: %s \r\n", request.c_str());
      ret = send(clientSocket, request.c_str(), request.length(), 0);
      if (ret > 0)
      {
        int bufLen = 1024 * 2;
        char bufRecv[bufLen] = {0};
        ret = recv(clientSocket, bufRecv, bufLen, 0);
        if (ret > 0)
        {
          response = bufRecv;
          printf("http post response: %s.\r\n", bufRecv);
        }
        else
        {
          nRet = -1;
          printf("\033[31m http post connect recv, code: %d. \r\n \033[0m", ret);
        }
      }
      else
      {
        nRet = -1;
        printf("\033[0m http post send error, code: %d. \r\n \033[0m", ret);
      }
    }
    else
    {
      nRet = -1;
      printf("\033[0m http post connect error, code: %d. \r\n \033[0m", ret);
    }
    close(clientSocket);
  }
  catch (...)
  {
    nRet = -1;
    printf("\033[0m http post error. \r\n \033[0m");
  }

  return nRet;
}

int CHttpRequest::put(std::string address, int port, std::string url, std::string headers, std::string data, std::string &response)
{
  int nRet = 0;
  try
  {
    int clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in ServerAddr;
    memset(&ServerAddr, 0, sizeof(ServerAddr));
    ServerAddr.sin_addr.s_addr = inet_addr(address.c_str());
    ServerAddr.sin_port = htons(port);
    ServerAddr.sin_family = AF_INET;
    int ret = connect(clientSocket, (sockaddr *)&ServerAddr, sizeof(ServerAddr));
    if (ret == 0)
    {
      int len = data.length();
      std::string request;
      request = boost::str(boost::format("PUT %s HTTP/1.1\r\nHost: %s:%d\r\n%sCookie: 16888\r\nAccept: application/json\r\nConnection: keep-alive\r\nContent-Type: application/json\r\nCharset: utf-8\r\nContent-Length: %d\r\n\r\n%s") % url % address % port % headers % len % data);
      printf("http put request: %s \r\n", request.c_str());
      ret = send(clientSocket, request.c_str(), request.length(), 0);
      if (ret > 0)
      {
        int bufLen = 1024 * 2;
        char bufRecv[bufLen] = {0};
        ret = recv(clientSocket, bufRecv, bufLen, 0);
        if (ret > 0)
        {
          response = bufRecv;
          printf("http put response: %s.\r\n", bufRecv);
        }
        else
        {
          nRet = -1;
          printf("\033[31m http put connect recv, code: %d. \r\n \033[0m", ret);
        }
      }
      else
      {
        nRet = -1;
        printf("\033[0m http put send error, code: %d. \r\n \033[0m", ret);
      }
    }
    else
    {
      nRet = -1;
      printf("\033[0m http put connect error, code: %d. \r\n \033[0m", ret);
    }
    close(clientSocket);
  }
  catch (...)
  {
    nRet = -1;
    printf("\033[0m http put error. \r\n \033[0m");
  }

  return nRet;
}